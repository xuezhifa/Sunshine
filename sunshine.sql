/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : sunshine

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2016-05-26 15:19:32
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for cmf_ad
-- ----------------------------
DROP TABLE IF EXISTS `cmf_ad`;
CREATE TABLE `cmf_ad` (
  `ad_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '广告id',
  `ad_name` varchar(255) NOT NULL COMMENT '广告名称',
  `ad_content` text COMMENT '广告内容',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  PRIMARY KEY (`ad_id`),
  KEY `ad_name` (`ad_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of cmf_ad
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_asset
-- ----------------------------
DROP TABLE IF EXISTS `cmf_asset`;
CREATE TABLE `cmf_asset` (
  `aid` bigint(20) NOT NULL AUTO_INCREMENT,
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '用户 id',
  `key` varchar(50) NOT NULL COMMENT '资源 key',
  `filename` varchar(50) DEFAULT NULL COMMENT '文件名',
  `filesize` int(11) DEFAULT NULL COMMENT '文件大小,单位Byte',
  `filepath` varchar(200) NOT NULL COMMENT '文件路径，相对于 upload 目录，可以为 url',
  `uploadtime` int(11) NOT NULL COMMENT '上传时间',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1：可用，0：删除，不可用',
  `meta` text COMMENT '其它详细信息，JSON格式',
  `suffix` varchar(50) DEFAULT NULL COMMENT '文件后缀名，不包括点',
  `download_times` int(11) NOT NULL DEFAULT '0' COMMENT '下载次数',
  PRIMARY KEY (`aid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='资源表';

-- ----------------------------
-- Records of cmf_asset
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_auth_access
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_access`;
CREATE TABLE `cmf_auth_access` (
  `role_id` mediumint(8) unsigned NOT NULL COMMENT '角色',
  `rule_name` varchar(255) NOT NULL COMMENT '规则唯一英文标识,全小写',
  `type` varchar(30) DEFAULT NULL COMMENT '权限规则分类，请加应用前缀,如admin_',
  KEY `role_id` (`role_id`),
  KEY `rule_name` (`rule_name`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='权限授权表';

-- ----------------------------
-- Records of cmf_auth_access
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_auth_rule
-- ----------------------------
DROP TABLE IF EXISTS `cmf_auth_rule`;
CREATE TABLE `cmf_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT COMMENT '规则id,自增主键',
  `module` varchar(20) NOT NULL COMMENT '规则所属module',
  `type` varchar(30) NOT NULL DEFAULT '1' COMMENT '权限规则分类，请加应用前缀,如admin_',
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '规则唯一英文标识,全小写',
  `param` varchar(255) DEFAULT NULL COMMENT '额外url参数',
  `title` varchar(20) NOT NULL DEFAULT '' COMMENT '规则中文描述',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '是否有效(0:无效,1:有效)',
  `condition` varchar(300) NOT NULL DEFAULT '' COMMENT '规则附加条件',
  PRIMARY KEY (`id`),
  KEY `module` (`module`,`status`,`type`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 COMMENT='权限规则表';

-- ----------------------------
-- Records of cmf_auth_rule
-- ----------------------------
INSERT INTO `cmf_auth_rule` VALUES ('1', 'Admin', 'admin_url', 'admin/content/default', null, '内容管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('2', 'Api', 'admin_url', 'api/guestbookadmin/index', null, '所有留言', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('3', 'Api', 'admin_url', 'api/guestbookadmin/delete', null, '删除网站留言', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('4', 'Comment', 'admin_url', 'comment/commentadmin/index', null, '评论管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('5', 'Comment', 'admin_url', 'comment/commentadmin/delete', null, '删除评论', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('6', 'Comment', 'admin_url', 'comment/commentadmin/check', null, '评论审核', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('7', 'Portal', 'admin_url', 'portal/adminpost/index', null, '文章管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('8', 'Portal', 'admin_url', 'portal/adminpost/listorders', null, '文章排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('9', 'Portal', 'admin_url', 'portal/adminpost/top', null, '文章置顶', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('10', 'Portal', 'admin_url', 'portal/adminpost/recommend', null, '文章推荐', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('11', 'Portal', 'admin_url', 'portal/adminpost/move', null, '批量移动', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('12', 'Portal', 'admin_url', 'portal/adminpost/check', null, '文章审核', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('13', 'Portal', 'admin_url', 'portal/adminpost/delete', null, '删除文章', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('14', 'Portal', 'admin_url', 'portal/adminpost/edit', null, '编辑文章', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('15', 'Portal', 'admin_url', 'portal/adminpost/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('16', 'Portal', 'admin_url', 'portal/adminpost/add', null, '添加文章', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('17', 'Portal', 'admin_url', 'portal/adminpost/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('18', 'Portal', 'admin_url', 'portal/adminterm/index', null, '分类管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('19', 'Portal', 'admin_url', 'portal/adminterm/listorders', null, '文章分类排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('20', 'Portal', 'admin_url', 'portal/adminterm/delete', null, '删除分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('21', 'Portal', 'admin_url', 'portal/adminterm/edit', null, '编辑分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('22', 'Portal', 'admin_url', 'portal/adminterm/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('23', 'Portal', 'admin_url', 'portal/adminterm/add', null, '添加分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('24', 'Portal', 'admin_url', 'portal/adminterm/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('25', 'Portal', 'admin_url', 'portal/adminpage/index', null, '页面管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('26', 'Portal', 'admin_url', 'portal/adminpage/listorders', null, '页面排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('27', 'Portal', 'admin_url', 'portal/adminpage/delete', null, '删除页面', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('28', 'Portal', 'admin_url', 'portal/adminpage/edit', null, '编辑页面', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('29', 'Portal', 'admin_url', 'portal/adminpage/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('30', 'Portal', 'admin_url', 'portal/adminpage/add', null, '添加页面', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('31', 'Portal', 'admin_url', 'portal/adminpage/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('32', 'Admin', 'admin_url', 'admin/recycle/default', null, '回收站', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('33', 'Portal', 'admin_url', 'portal/adminpost/recyclebin', null, '文章回收', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('34', 'Portal', 'admin_url', 'portal/adminpost/restore', null, '文章还原', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('35', 'Portal', 'admin_url', 'portal/adminpost/clean', null, '彻底删除', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('36', 'Portal', 'admin_url', 'portal/adminpage/recyclebin', null, '页面回收', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('37', 'Portal', 'admin_url', 'portal/adminpage/clean', null, '彻底删除', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('38', 'Portal', 'admin_url', 'portal/adminpage/restore', null, '页面还原', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('39', 'Admin', 'admin_url', 'admin/extension/default', null, '扩展工具', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('40', 'Admin', 'admin_url', 'admin/backup/default', null, '备份管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('41', 'Admin', 'admin_url', 'admin/backup/restore', null, '数据还原', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('42', 'Admin', 'admin_url', 'admin/backup/index', null, '数据备份', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('43', 'Admin', 'admin_url', 'admin/backup/index_post', null, '提交数据备份', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('44', 'Admin', 'admin_url', 'admin/backup/download', null, '下载备份', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('45', 'Admin', 'admin_url', 'admin/backup/del_backup', null, '删除备份', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('46', 'Admin', 'admin_url', 'admin/backup/import', null, '数据备份导入', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('47', 'Admin', 'admin_url', 'admin/plugin/index', null, '插件管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('48', 'Admin', 'admin_url', 'admin/plugin/toggle', null, '插件启用切换', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('49', 'Admin', 'admin_url', 'admin/plugin/setting', null, '插件设置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('50', 'Admin', 'admin_url', 'admin/plugin/setting_post', null, '插件设置提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('51', 'Admin', 'admin_url', 'admin/plugin/install', null, '插件安装', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('52', 'Admin', 'admin_url', 'admin/plugin/uninstall', null, '插件卸载', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('53', 'Admin', 'admin_url', 'admin/slide/default', null, '幻灯片', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('54', 'Admin', 'admin_url', 'admin/slide/index', null, '幻灯片管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('55', 'Admin', 'admin_url', 'admin/slide/listorders', null, '幻灯片排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('56', 'Admin', 'admin_url', 'admin/slide/toggle', null, '幻灯片显示切换', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('57', 'Admin', 'admin_url', 'admin/slide/delete', null, '删除幻灯片', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('58', 'Admin', 'admin_url', 'admin/slide/edit', null, '编辑幻灯片', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('59', 'Admin', 'admin_url', 'admin/slide/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('60', 'Admin', 'admin_url', 'admin/slide/add', null, '添加幻灯片', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('61', 'Admin', 'admin_url', 'admin/slide/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('62', 'Admin', 'admin_url', 'admin/slidecat/index', null, '幻灯片分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('63', 'Admin', 'admin_url', 'admin/slidecat/delete', null, '删除分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('64', 'Admin', 'admin_url', 'admin/slidecat/edit', null, '编辑分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('65', 'Admin', 'admin_url', 'admin/slidecat/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('66', 'Admin', 'admin_url', 'admin/slidecat/add', null, '添加分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('67', 'Admin', 'admin_url', 'admin/slidecat/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('68', 'Admin', 'admin_url', 'admin/ad/index', null, '网站广告', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('69', 'Admin', 'admin_url', 'admin/ad/toggle', null, '广告显示切换', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('70', 'Admin', 'admin_url', 'admin/ad/delete', null, '删除广告', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('71', 'Admin', 'admin_url', 'admin/ad/edit', null, '编辑广告', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('72', 'Admin', 'admin_url', 'admin/ad/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('73', 'Admin', 'admin_url', 'admin/ad/add', null, '添加广告', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('74', 'Admin', 'admin_url', 'admin/ad/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('75', 'Admin', 'admin_url', 'admin/link/index', null, '友情链接', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('76', 'Admin', 'admin_url', 'admin/link/listorders', null, '友情链接排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('77', 'Admin', 'admin_url', 'admin/link/toggle', null, '友链显示切换', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('78', 'Admin', 'admin_url', 'admin/link/delete', null, '删除友情链接', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('79', 'Admin', 'admin_url', 'admin/link/edit', null, '编辑友情链接', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('80', 'Admin', 'admin_url', 'admin/link/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('81', 'Admin', 'admin_url', 'admin/link/add', null, '添加友情链接', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('82', 'Admin', 'admin_url', 'admin/link/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('83', 'Api', 'admin_url', 'api/oauthadmin/setting', null, '第三方登陆', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('84', 'Api', 'admin_url', 'api/oauthadmin/setting_post', null, '提交设置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('85', 'Admin', 'admin_url', 'admin/menu/default', null, '菜单管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('86', 'Admin', 'admin_url', 'admin/navcat/default1', null, '前台菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('87', 'Admin', 'admin_url', 'admin/nav/index', null, '菜单管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('88', 'Admin', 'admin_url', 'admin/nav/listorders', null, '前台导航排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('89', 'Admin', 'admin_url', 'admin/nav/delete', null, '删除菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('90', 'Admin', 'admin_url', 'admin/nav/edit', null, '编辑菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('91', 'Admin', 'admin_url', 'admin/nav/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('92', 'Admin', 'admin_url', 'admin/nav/add', null, '添加菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('93', 'Admin', 'admin_url', 'admin/nav/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('94', 'Admin', 'admin_url', 'admin/navcat/index', null, '菜单分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('95', 'Admin', 'admin_url', 'admin/navcat/delete', null, '删除分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('96', 'Admin', 'admin_url', 'admin/navcat/edit', null, '编辑分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('97', 'Admin', 'admin_url', 'admin/navcat/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('98', 'Admin', 'admin_url', 'admin/navcat/add', null, '添加分类', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('99', 'Admin', 'admin_url', 'admin/navcat/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('100', 'Admin', 'admin_url', 'admin/menu/index', null, '后台菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('101', 'Admin', 'admin_url', 'admin/menu/add', null, '添加菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('102', 'Admin', 'admin_url', 'admin/menu/add_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('103', 'Admin', 'admin_url', 'admin/menu/listorders', null, '后台菜单排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('104', 'Admin', 'admin_url', 'admin/menu/export_menu', null, '菜单备份', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('105', 'Admin', 'admin_url', 'admin/menu/edit', null, '编辑菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('106', 'Admin', 'admin_url', 'admin/menu/edit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('107', 'Admin', 'admin_url', 'admin/menu/delete', null, '删除菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('108', 'Admin', 'admin_url', 'admin/menu/lists', null, '所有菜单', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('109', 'Admin', 'admin_url', 'admin/setting/default', null, '设置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('110', 'Admin', 'admin_url', 'admin/setting/userdefault', null, '个人信息', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('111', 'Admin', 'admin_url', 'admin/user/userinfo', null, '修改信息', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('112', 'Admin', 'admin_url', 'admin/user/userinfo_post', null, '修改信息提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('113', 'Admin', 'admin_url', 'admin/setting/password', null, '修改密码', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('114', 'Admin', 'admin_url', 'admin/setting/password_post', null, '提交修改', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('115', 'Admin', 'admin_url', 'admin/setting/site', null, '网站信息', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('116', 'Admin', 'admin_url', 'admin/setting/site_post', null, '提交修改', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('117', 'Admin', 'admin_url', 'admin/route/index', null, '路由列表', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('118', 'Admin', 'admin_url', 'admin/route/add', null, '路由添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('119', 'Admin', 'admin_url', 'admin/route/add_post', null, '路由添加提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('120', 'Admin', 'admin_url', 'admin/route/edit', null, '路由编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('121', 'Admin', 'admin_url', 'admin/route/edit_post', null, '路由编辑提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('122', 'Admin', 'admin_url', 'admin/route/delete', null, '路由删除', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('123', 'Admin', 'admin_url', 'admin/route/ban', null, '路由禁止', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('124', 'Admin', 'admin_url', 'admin/route/open', null, '路由启用', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('125', 'Admin', 'admin_url', 'admin/route/listorders', null, '路由排序', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('126', 'Admin', 'admin_url', 'admin/mailer/default', null, '邮箱配置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('127', 'Admin', 'admin_url', 'admin/mailer/index', null, 'SMTP配置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('128', 'Admin', 'admin_url', 'admin/mailer/index_post', null, '提交配置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('129', 'Admin', 'admin_url', 'admin/mailer/active', null, '邮件模板', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('130', 'Admin', 'admin_url', 'admin/mailer/active_post', null, '提交模板', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('131', 'Admin', 'admin_url', 'admin/setting/clearcache', null, '清除缓存', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('132', 'User', 'admin_url', 'user/indexadmin/default', null, '用户管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('133', 'User', 'admin_url', 'user/indexadmin/default1', null, '用户组', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('134', 'User', 'admin_url', 'user/indexadmin/index', null, '本站用户', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('135', 'User', 'admin_url', 'user/indexadmin/ban', null, '拉黑会员', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('136', 'User', 'admin_url', 'user/indexadmin/cancelban', null, '启用会员', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('137', 'User', 'admin_url', 'user/oauthadmin/index', null, '第三方用户', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('138', 'User', 'admin_url', 'user/oauthadmin/delete', null, '第三方用户解绑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('139', 'User', 'admin_url', 'user/indexadmin/default3', null, '管理组', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('140', 'Admin', 'admin_url', 'admin/rbac/index', null, '角色管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('141', 'Admin', 'admin_url', 'admin/rbac/member', null, '成员管理', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('142', 'Admin', 'admin_url', 'admin/rbac/authorize', null, '权限设置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('143', 'Admin', 'admin_url', 'admin/rbac/authorize_post', null, '提交设置', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('144', 'Admin', 'admin_url', 'admin/rbac/roleedit', null, '编辑角色', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('145', 'Admin', 'admin_url', 'admin/rbac/roleedit_post', null, '提交编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('146', 'Admin', 'admin_url', 'admin/rbac/roledelete', null, '删除角色', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('147', 'Admin', 'admin_url', 'admin/rbac/roleadd', null, '添加角色', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('148', 'Admin', 'admin_url', 'admin/rbac/roleadd_post', null, '提交添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('149', 'Admin', 'admin_url', 'admin/user/index', null, '管理员', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('150', 'Admin', 'admin_url', 'admin/user/delete', null, '删除管理员', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('151', 'Admin', 'admin_url', 'admin/user/edit', null, '管理员编辑', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('152', 'Admin', 'admin_url', 'admin/user/edit_post', null, '编辑提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('153', 'Admin', 'admin_url', 'admin/user/add', null, '管理员添加', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('154', 'Admin', 'admin_url', 'admin/user/add_post', null, '添加提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('155', 'Admin', 'admin_url', 'admin/plugin/update', null, '插件更新', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('156', 'Admin', 'admin_url', 'admin/storage/index', null, '文件存储', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('157', 'Admin', 'admin_url', 'admin/storage/setting_post', null, '文件存储设置提交', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('158', 'Admin', 'admin_url', 'admin/slide/ban', null, '禁用幻灯片', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('159', 'Admin', 'admin_url', 'admin/slide/cancelban', null, '启用幻灯片', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('160', 'Admin', 'admin_url', 'admin/user/ban', null, '禁用管理员', '1', '');
INSERT INTO `cmf_auth_rule` VALUES ('161', 'Admin', 'admin_url', 'admin/user/cancelban', null, '启用管理员', '1', '');

-- ----------------------------
-- Table structure for cmf_comments
-- ----------------------------
DROP TABLE IF EXISTS `cmf_comments`;
CREATE TABLE `cmf_comments` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_table` varchar(100) NOT NULL COMMENT '评论内容所在表，不带表前缀',
  `post_id` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '评论内容 id',
  `url` varchar(255) DEFAULT NULL COMMENT '原文地址',
  `uid` int(11) NOT NULL DEFAULT '0' COMMENT '发表评论的用户id',
  `to_uid` int(11) NOT NULL DEFAULT '0' COMMENT '被评论的用户id',
  `full_name` varchar(50) DEFAULT NULL COMMENT '评论者昵称',
  `email` varchar(255) DEFAULT NULL COMMENT '评论者邮箱',
  `createtime` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '评论时间',
  `content` text NOT NULL COMMENT '评论内容',
  `type` smallint(1) NOT NULL DEFAULT '1' COMMENT '评论类型；1实名评论',
  `parentid` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '被回复的评论id',
  `path` varchar(500) DEFAULT NULL,
  `status` smallint(1) NOT NULL DEFAULT '1' COMMENT '状态，1已审核，0未审核',
  PRIMARY KEY (`id`),
  KEY `comment_post_ID` (`post_id`),
  KEY `comment_approved_date_gmt` (`status`),
  KEY `comment_parent` (`parentid`),
  KEY `table_id_status` (`post_table`,`post_id`,`status`),
  KEY `createtime` (`createtime`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='评论表';

-- ----------------------------
-- Records of cmf_comments
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_common_action_log
-- ----------------------------
DROP TABLE IF EXISTS `cmf_common_action_log`;
CREATE TABLE `cmf_common_action_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` bigint(20) DEFAULT '0' COMMENT '用户id',
  `object` varchar(100) DEFAULT NULL COMMENT '访问对象的id,格式：不带前缀的表名+id;如posts1表示xx_posts表里id为1的记录',
  `action` varchar(50) DEFAULT NULL COMMENT '操作名称；格式规定为：应用名+控制器+操作名；也可自己定义格式只要不发生冲突且惟一；',
  `count` int(11) DEFAULT '0' COMMENT '访问次数',
  `last_time` int(11) DEFAULT '0' COMMENT '最后访问的时间戳',
  `ip` varchar(15) DEFAULT NULL COMMENT '访问者最后访问ip',
  PRIMARY KEY (`id`),
  KEY `user_object_action` (`user`,`object`,`action`),
  KEY `user_object_action_ip` (`user`,`object`,`action`,`ip`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='访问记录表';

-- ----------------------------
-- Records of cmf_common_action_log
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_guestbook
-- ----------------------------
DROP TABLE IF EXISTS `cmf_guestbook`;
CREATE TABLE `cmf_guestbook` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `full_name` varchar(50) NOT NULL COMMENT '留言者姓名',
  `email` varchar(100) NOT NULL COMMENT '留言者邮箱',
  `title` varchar(255) DEFAULT NULL COMMENT '留言标题',
  `msg` text NOT NULL COMMENT '留言内容',
  `createtime` datetime NOT NULL COMMENT '留言时间',
  `status` smallint(2) NOT NULL DEFAULT '1' COMMENT '留言状态，1：正常，0：删除',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='留言表';

-- ----------------------------
-- Records of cmf_guestbook
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_links
-- ----------------------------
DROP TABLE IF EXISTS `cmf_links`;
CREATE TABLE `cmf_links` (
  `link_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) NOT NULL COMMENT '友情链接地址',
  `link_name` varchar(255) NOT NULL COMMENT '友情链接名称',
  `link_image` varchar(255) DEFAULT NULL COMMENT '友情链接图标',
  `link_target` varchar(25) NOT NULL DEFAULT '_blank' COMMENT '友情链接打开方式',
  `link_description` text NOT NULL COMMENT '友情链接描述',
  `link_status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  `link_rating` int(11) NOT NULL DEFAULT '0' COMMENT '友情链接评级',
  `link_rel` varchar(255) DEFAULT NULL COMMENT '链接与网站的关系',
  `listorder` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_status`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='友情链接表';

-- ----------------------------
-- Records of cmf_links
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_menu
-- ----------------------------
DROP TABLE IF EXISTS `cmf_menu`;
CREATE TABLE `cmf_menu` (
  `id` smallint(6) unsigned NOT NULL AUTO_INCREMENT,
  `parentid` smallint(6) unsigned NOT NULL DEFAULT '0',
  `app` char(20) NOT NULL COMMENT '应用名称app',
  `model` char(20) NOT NULL COMMENT '控制器',
  `action` char(20) NOT NULL COMMENT '操作名称',
  `data` char(50) NOT NULL COMMENT '额外参数',
  `type` tinyint(1) NOT NULL DEFAULT '0' COMMENT '菜单类型  1：权限认证+菜单；0：只作为菜单',
  `status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '状态，1显示，0不显示',
  `name` varchar(50) NOT NULL COMMENT '菜单名称',
  `icon` varchar(50) DEFAULT NULL COMMENT '菜单图标',
  `remark` varchar(255) NOT NULL COMMENT '备注',
  `listorder` smallint(6) unsigned NOT NULL DEFAULT '0' COMMENT '排序ID',
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `parentid` (`parentid`),
  KEY `model` (`model`)
) ENGINE=MyISAM AUTO_INCREMENT=162 DEFAULT CHARSET=utf8 COMMENT='后台菜单表';

-- ----------------------------
-- Records of cmf_menu
-- ----------------------------
INSERT INTO `cmf_menu` VALUES ('1', '0', 'Admin', 'Content', 'default', '', '0', '1', '内容管理', 'th', '', '30');
INSERT INTO `cmf_menu` VALUES ('2', '1', 'Api', 'Guestbookadmin', 'index', '', '1', '0', '所有留言', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('3', '2', 'Api', 'Guestbookadmin', 'delete', '', '1', '0', '删除网站留言', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('4', '1', 'Comment', 'Commentadmin', 'index', '', '1', '0', '评论管理', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('5', '4', 'Comment', 'Commentadmin', 'delete', '', '1', '0', '删除评论', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('6', '4', 'Comment', 'Commentadmin', 'check', '', '1', '0', '评论审核', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('7', '1', 'Portal', 'AdminPost', 'index', '', '1', '1', '文章管理', '', '', '1');
INSERT INTO `cmf_menu` VALUES ('8', '7', 'Portal', 'AdminPost', 'listorders', '', '1', '0', '文章排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('9', '7', 'Portal', 'AdminPost', 'top', '', '1', '0', '文章置顶', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('10', '7', 'Portal', 'AdminPost', 'recommend', '', '1', '0', '文章推荐', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('11', '7', 'Portal', 'AdminPost', 'move', '', '1', '0', '批量移动', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('12', '7', 'Portal', 'AdminPost', 'check', '', '1', '0', '文章审核', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('13', '7', 'Portal', 'AdminPost', 'delete', '', '1', '0', '删除文章', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('14', '7', 'Portal', 'AdminPost', 'edit', '', '1', '0', '编辑文章', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('15', '14', 'Portal', 'AdminPost', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('16', '7', 'Portal', 'AdminPost', 'add', '', '1', '0', '添加文章', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('17', '16', 'Portal', 'AdminPost', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('18', '1', 'Portal', 'AdminTerm', 'index', '', '0', '1', '分类管理', '', '', '2');
INSERT INTO `cmf_menu` VALUES ('19', '18', 'Portal', 'AdminTerm', 'listorders', '', '1', '0', '文章分类排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('20', '18', 'Portal', 'AdminTerm', 'delete', '', '1', '0', '删除分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('21', '18', 'Portal', 'AdminTerm', 'edit', '', '1', '0', '编辑分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('22', '21', 'Portal', 'AdminTerm', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('23', '18', 'Portal', 'AdminTerm', 'add', '', '1', '0', '添加分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('24', '23', 'Portal', 'AdminTerm', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('25', '1', 'Portal', 'AdminPage', 'index', '', '1', '1', '页面管理', '', '', '3');
INSERT INTO `cmf_menu` VALUES ('26', '25', 'Portal', 'AdminPage', 'listorders', '', '1', '0', '页面排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('27', '25', 'Portal', 'AdminPage', 'delete', '', '1', '0', '删除页面', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('28', '25', 'Portal', 'AdminPage', 'edit', '', '1', '0', '编辑页面', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('29', '28', 'Portal', 'AdminPage', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('30', '25', 'Portal', 'AdminPage', 'add', '', '1', '0', '添加页面', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('31', '30', 'Portal', 'AdminPage', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('32', '1', 'Admin', 'Recycle', 'default', '', '1', '1', '回收站', '', '', '4');
INSERT INTO `cmf_menu` VALUES ('33', '32', 'Portal', 'AdminPost', 'recyclebin', '', '1', '1', '文章回收', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('34', '33', 'Portal', 'AdminPost', 'restore', '', '1', '0', '文章还原', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('35', '33', 'Portal', 'AdminPost', 'clean', '', '1', '0', '彻底删除', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('36', '32', 'Portal', 'AdminPage', 'recyclebin', '', '1', '1', '页面回收', '', '', '1');
INSERT INTO `cmf_menu` VALUES ('37', '36', 'Portal', 'AdminPage', 'clean', '', '1', '0', '彻底删除', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('38', '36', 'Portal', 'AdminPage', 'restore', '', '1', '0', '页面还原', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('39', '0', 'Admin', 'Extension', 'default', '', '0', '1', '扩展工具', 'cloud', '', '40');
INSERT INTO `cmf_menu` VALUES ('40', '39', 'Admin', 'Backup', 'default', '', '1', '1', '备份管理', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('41', '40', 'Admin', 'Backup', 'restore', '', '1', '1', '数据还原', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('42', '40', 'Admin', 'Backup', 'index', '', '1', '1', '数据备份', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('43', '42', 'Admin', 'Backup', 'index_post', '', '1', '0', '提交数据备份', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('44', '40', 'Admin', 'Backup', 'download', '', '1', '0', '下载备份', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('45', '40', 'Admin', 'Backup', 'del_backup', '', '1', '0', '删除备份', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('46', '40', 'Admin', 'Backup', 'import', '', '1', '0', '数据备份导入', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('47', '39', 'Admin', 'Plugin', 'index', '', '1', '1', '插件管理', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('48', '47', 'Admin', 'Plugin', 'toggle', '', '1', '0', '插件启用切换', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('49', '47', 'Admin', 'Plugin', 'setting', '', '1', '0', '插件设置', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('50', '49', 'Admin', 'Plugin', 'setting_post', '', '1', '0', '插件设置提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('51', '47', 'Admin', 'Plugin', 'install', '', '1', '0', '插件安装', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('52', '47', 'Admin', 'Plugin', 'uninstall', '', '1', '0', '插件卸载', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('53', '39', 'Admin', 'Slide', 'default', '', '1', '1', '幻灯片', '', '', '1');
INSERT INTO `cmf_menu` VALUES ('54', '53', 'Admin', 'Slide', 'index', '', '1', '1', '幻灯片管理', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('55', '54', 'Admin', 'Slide', 'listorders', '', '1', '0', '幻灯片排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('56', '54', 'Admin', 'Slide', 'toggle', '', '1', '0', '幻灯片显示切换', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('57', '54', 'Admin', 'Slide', 'delete', '', '1', '0', '删除幻灯片', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('58', '54', 'Admin', 'Slide', 'edit', '', '1', '0', '编辑幻灯片', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('59', '58', 'Admin', 'Slide', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('60', '54', 'Admin', 'Slide', 'add', '', '1', '0', '添加幻灯片', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('61', '60', 'Admin', 'Slide', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('62', '53', 'Admin', 'Slidecat', 'index', '', '1', '1', '幻灯片分类', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('63', '62', 'Admin', 'Slidecat', 'delete', '', '1', '0', '删除分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('64', '62', 'Admin', 'Slidecat', 'edit', '', '1', '0', '编辑分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('65', '64', 'Admin', 'Slidecat', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('66', '62', 'Admin', 'Slidecat', 'add', '', '1', '0', '添加分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('67', '66', 'Admin', 'Slidecat', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('68', '39', 'Admin', 'Ad', 'index', '', '1', '0', '网站广告', '', '', '2');
INSERT INTO `cmf_menu` VALUES ('69', '68', 'Admin', 'Ad', 'toggle', '', '1', '0', '广告显示切换', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('70', '68', 'Admin', 'Ad', 'delete', '', '1', '0', '删除广告', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('71', '68', 'Admin', 'Ad', 'edit', '', '1', '0', '编辑广告', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('72', '71', 'Admin', 'Ad', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('73', '68', 'Admin', 'Ad', 'add', '', '1', '0', '添加广告', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('74', '73', 'Admin', 'Ad', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('75', '39', 'Admin', 'Link', 'index', '', '0', '0', '友情链接', '', '', '3');
INSERT INTO `cmf_menu` VALUES ('76', '75', 'Admin', 'Link', 'listorders', '', '1', '0', '友情链接排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('77', '75', 'Admin', 'Link', 'toggle', '', '1', '0', '友链显示切换', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('78', '75', 'Admin', 'Link', 'delete', '', '1', '0', '删除友情链接', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('79', '75', 'Admin', 'Link', 'edit', '', '1', '0', '编辑友情链接', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('80', '79', 'Admin', 'Link', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('81', '75', 'Admin', 'Link', 'add', '', '1', '0', '添加友情链接', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('82', '81', 'Admin', 'Link', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('83', '39', 'Api', 'Oauthadmin', 'setting', '', '1', '1', '第三方登陆', 'leaf', '', '4');
INSERT INTO `cmf_menu` VALUES ('84', '83', 'Api', 'Oauthadmin', 'setting_post', '', '1', '0', '提交设置', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('85', '0', 'Admin', 'Menu', 'default', '', '1', '1', '菜单管理', 'list', '', '20');
INSERT INTO `cmf_menu` VALUES ('86', '85', 'Admin', 'Navcat', 'default1', '', '1', '1', '前台菜单', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('87', '86', 'Admin', 'Nav', 'index', '', '1', '1', '菜单管理', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('88', '87', 'Admin', 'Nav', 'listorders', '', '1', '0', '前台导航排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('89', '87', 'Admin', 'Nav', 'delete', '', '1', '0', '删除菜单', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('90', '87', 'Admin', 'Nav', 'edit', '', '1', '0', '编辑菜单', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('91', '90', 'Admin', 'Nav', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('92', '87', 'Admin', 'Nav', 'add', '', '1', '0', '添加菜单', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('93', '92', 'Admin', 'Nav', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('94', '86', 'Admin', 'Navcat', 'index', '', '1', '1', '菜单分类', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('95', '94', 'Admin', 'Navcat', 'delete', '', '1', '0', '删除分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('96', '94', 'Admin', 'Navcat', 'edit', '', '1', '0', '编辑分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('97', '96', 'Admin', 'Navcat', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('98', '94', 'Admin', 'Navcat', 'add', '', '1', '0', '添加分类', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('99', '98', 'Admin', 'Navcat', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('100', '85', 'Admin', 'Menu', 'index', '', '1', '1', '后台菜单', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('101', '100', 'Admin', 'Menu', 'add', '', '1', '0', '添加菜单', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('102', '101', 'Admin', 'Menu', 'add_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('103', '100', 'Admin', 'Menu', 'listorders', '', '1', '0', '后台菜单排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('104', '100', 'Admin', 'Menu', 'export_menu', '', '1', '0', '菜单备份', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('105', '100', 'Admin', 'Menu', 'edit', '', '1', '0', '编辑菜单', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('106', '105', 'Admin', 'Menu', 'edit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('107', '100', 'Admin', 'Menu', 'delete', '', '1', '0', '删除菜单', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('108', '100', 'Admin', 'Menu', 'lists', '', '1', '0', '所有菜单', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('109', '0', 'Admin', 'Setting', 'default', '', '0', '1', '设置', 'cogs', '', '0');
INSERT INTO `cmf_menu` VALUES ('110', '109', 'Admin', 'Setting', 'userdefault', '', '0', '1', '个人信息', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('111', '110', 'Admin', 'User', 'userinfo', '', '1', '1', '修改信息', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('112', '111', 'Admin', 'User', 'userinfo_post', '', '1', '0', '修改信息提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('113', '110', 'Admin', 'Setting', 'password', '', '1', '1', '修改密码', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('114', '113', 'Admin', 'Setting', 'password_post', '', '1', '0', '提交修改', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('115', '109', 'Admin', 'Setting', 'site', '', '1', '1', '网站信息', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('116', '115', 'Admin', 'Setting', 'site_post', '', '1', '0', '提交修改', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('117', '115', 'Admin', 'Route', 'index', '', '1', '0', '路由列表', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('118', '115', 'Admin', 'Route', 'add', '', '1', '0', '路由添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('119', '118', 'Admin', 'Route', 'add_post', '', '1', '0', '路由添加提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('120', '115', 'Admin', 'Route', 'edit', '', '1', '0', '路由编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('121', '120', 'Admin', 'Route', 'edit_post', '', '1', '0', '路由编辑提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('122', '115', 'Admin', 'Route', 'delete', '', '1', '0', '路由删除', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('123', '115', 'Admin', 'Route', 'ban', '', '1', '0', '路由禁止', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('124', '115', 'Admin', 'Route', 'open', '', '1', '0', '路由启用', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('125', '115', 'Admin', 'Route', 'listorders', '', '1', '0', '路由排序', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('126', '109', 'Admin', 'Mailer', 'default', '', '1', '1', '邮箱配置', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('127', '126', 'Admin', 'Mailer', 'index', '', '1', '1', 'SMTP配置', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('128', '127', 'Admin', 'Mailer', 'index_post', '', '1', '0', '提交配置', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('129', '126', 'Admin', 'Mailer', 'active', '', '1', '1', '邮件模板', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('130', '129', 'Admin', 'Mailer', 'active_post', '', '1', '0', '提交模板', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('131', '109', 'Admin', 'Setting', 'clearcache', '', '1', '1', '清除缓存', '', '', '1');
INSERT INTO `cmf_menu` VALUES ('132', '0', 'User', 'Indexadmin', 'default', '', '1', '1', '用户管理', 'group', '', '10');
INSERT INTO `cmf_menu` VALUES ('133', '132', 'User', 'Indexadmin', 'default1', '', '1', '1', '用户组', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('134', '133', 'User', 'Indexadmin', 'index', '', '1', '1', '本站用户', 'leaf', '', '0');
INSERT INTO `cmf_menu` VALUES ('135', '134', 'User', 'Indexadmin', 'ban', '', '1', '0', '拉黑会员', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('136', '134', 'User', 'Indexadmin', 'cancelban', '', '1', '0', '启用会员', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('137', '133', 'User', 'Oauthadmin', 'index', '', '1', '1', '第三方用户', 'leaf', '', '0');
INSERT INTO `cmf_menu` VALUES ('138', '137', 'User', 'Oauthadmin', 'delete', '', '1', '0', '第三方用户解绑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('139', '132', 'User', 'Indexadmin', 'default3', '', '1', '1', '管理组', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('140', '139', 'Admin', 'Rbac', 'index', '', '1', '1', '角色管理', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('141', '140', 'Admin', 'Rbac', 'member', '', '1', '0', '成员管理', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('142', '140', 'Admin', 'Rbac', 'authorize', '', '1', '0', '权限设置', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('143', '142', 'Admin', 'Rbac', 'authorize_post', '', '1', '0', '提交设置', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('144', '140', 'Admin', 'Rbac', 'roleedit', '', '1', '0', '编辑角色', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('145', '144', 'Admin', 'Rbac', 'roleedit_post', '', '1', '0', '提交编辑', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('146', '140', 'Admin', 'Rbac', 'roledelete', '', '1', '1', '删除角色', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('147', '140', 'Admin', 'Rbac', 'roleadd', '', '1', '1', '添加角色', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('148', '147', 'Admin', 'Rbac', 'roleadd_post', '', '1', '0', '提交添加', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('149', '139', 'Admin', 'User', 'index', '', '1', '1', '管理员', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('150', '149', 'Admin', 'User', 'delete', '', '1', '0', '删除管理员', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('151', '149', 'Admin', 'User', 'edit', '', '1', '0', '管理员编辑', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('152', '151', 'Admin', 'User', 'edit_post', '', '1', '0', '编辑提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('153', '149', 'Admin', 'User', 'add', '', '1', '0', '管理员添加', '', '', '1000');
INSERT INTO `cmf_menu` VALUES ('154', '153', 'Admin', 'User', 'add_post', '', '1', '0', '添加提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('155', '47', 'Admin', 'Plugin', 'update', '', '1', '0', '插件更新', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('156', '39', 'Admin', 'Storage', 'index', '', '1', '0', '文件存储', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('157', '156', 'Admin', 'Storage', 'setting_post', '', '1', '0', '文件存储设置提交', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('158', '54', 'Admin', 'Slide', 'ban', '', '1', '0', '禁用幻灯片', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('159', '54', 'Admin', 'Slide', 'cancelban', '', '1', '0', '启用幻灯片', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('160', '149', 'Admin', 'User', 'ban', '', '1', '0', '禁用管理员', '', '', '0');
INSERT INTO `cmf_menu` VALUES ('161', '149', 'Admin', 'User', 'cancelban', '', '1', '0', '启用管理员', '', '', '0');

-- ----------------------------
-- Table structure for cmf_nav
-- ----------------------------
DROP TABLE IF EXISTS `cmf_nav`;
CREATE TABLE `cmf_nav` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL COMMENT '导航分类 id',
  `parentid` int(11) NOT NULL COMMENT '导航父 id',
  `label` varchar(255) NOT NULL COMMENT '导航标题',
  `target` varchar(50) DEFAULT NULL COMMENT '打开方式',
  `href` varchar(255) NOT NULL COMMENT '导航链接',
  `icon` varchar(255) NOT NULL COMMENT '导航图标',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  `listorder` int(6) DEFAULT '0' COMMENT '排序',
  `path` varchar(255) NOT NULL DEFAULT '0' COMMENT '层级关系',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COMMENT='前台导航表';

-- ----------------------------
-- Records of cmf_nav
-- ----------------------------
INSERT INTO `cmf_nav` VALUES ('1', '1', '0', '首页', '', 'home', '', '1', '0', '0-1');
INSERT INTO `cmf_nav` VALUES ('2', '1', '0', '公司资讯', '', 'a:2:{s:6:\"action\";s:17:\"Portal/List/index\";s:5:\"param\";a:1:{s:2:\"id\";s:1:\"3\";}}', '', '1', '0', '0-2');
INSERT INTO `cmf_nav` VALUES ('3', '1', '0', '影视剧集', '', 'a:2:{s:6:\"action\";s:17:\"Portal/Page/index\";s:5:\"param\";a:1:{s:2:\"id\";s:2:\"16\";}}', '', '1', '0', '0-3');
INSERT INTO `cmf_nav` VALUES ('4', '1', '0', '明星档案', '', 'a:2:{s:6:\"action\";s:17:\"Portal/Page/index\";s:5:\"param\";a:1:{s:2:\"id\";s:1:\"3\";}}', '', '1', '0', '0-4');
INSERT INTO `cmf_nav` VALUES ('5', '1', '0', '业务', '', 'home', '', '1', '0', '0-5');
INSERT INTO `cmf_nav` VALUES ('6', '1', '0', '商城', '', 'home', '', '1', '0', '0-6');
INSERT INTO `cmf_nav` VALUES ('7', '1', '0', '关于我们', '', 'home', '', '1', '0', '0-7');
INSERT INTO `cmf_nav` VALUES ('8', '1', '5', '招商信息', '', 'home', '', '1', '0', '0-5-8');
INSERT INTO `cmf_nav` VALUES ('9', '1', '5', '招聘', '', 'home', '', '1', '0', '0-5-9');
INSERT INTO `cmf_nav` VALUES ('10', '1', '5', '投资', '', 'home', '', '1', '0', '0-5-10');
INSERT INTO `cmf_nav` VALUES ('11', '1', '5', '投稿', '', 'home', '', '1', '0', '0-5-11');
INSERT INTO `cmf_nav` VALUES ('12', '1', '6', '正午阳光官方商店', '', 'home', '', '1', '0', '0-6-12');
INSERT INTO `cmf_nav` VALUES ('13', '1', '6', '王凯官方商店', '', 'home', '', '1', '0', '0-6-13');
INSERT INTO `cmf_nav` VALUES ('14', '1', '6', '郭涛官方商店', '', 'home', '', '1', '0', '0-6-14');
INSERT INTO `cmf_nav` VALUES ('15', '1', '6', '靳东官方商店', '', 'home', '', '1', '0', '0-6-15');
INSERT INTO `cmf_nav` VALUES ('16', '1', '6', '衍生品众筹平台', '', 'home', '', '1', '0', '0-6-16');
INSERT INTO `cmf_nav` VALUES ('17', '1', '7', '公司介绍', '', 'a:2:{s:6:\"action\";s:17:\"Portal/Page/index\";s:5:\"param\";a:1:{s:2:\"id\";s:1:\"2\";}}', '', '1', '0', '0-7-17');
INSERT INTO `cmf_nav` VALUES ('18', '1', '7', '团队介绍', '', 'home', '', '1', '0', '0-7-18');

-- ----------------------------
-- Table structure for cmf_nav_cat
-- ----------------------------
DROP TABLE IF EXISTS `cmf_nav_cat`;
CREATE TABLE `cmf_nav_cat` (
  `navcid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL COMMENT '导航分类名',
  `active` int(1) NOT NULL DEFAULT '1' COMMENT '是否为主菜单，1是，0不是',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`navcid`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='前台导航分类表';

-- ----------------------------
-- Records of cmf_nav_cat
-- ----------------------------
INSERT INTO `cmf_nav_cat` VALUES ('1', '主导航', '1', '主导航');

-- ----------------------------
-- Table structure for cmf_oauth_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_oauth_user`;
CREATE TABLE `cmf_oauth_user` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `from` varchar(20) NOT NULL COMMENT '用户来源key',
  `name` varchar(30) NOT NULL COMMENT '第三方昵称',
  `head_img` varchar(200) NOT NULL COMMENT '头像',
  `uid` int(20) NOT NULL COMMENT '关联的本站用户id',
  `create_time` datetime NOT NULL COMMENT '绑定时间',
  `last_login_time` datetime NOT NULL COMMENT '最后登录时间',
  `last_login_ip` varchar(16) NOT NULL COMMENT '最后登录ip',
  `login_times` int(6) NOT NULL COMMENT '登录次数',
  `status` tinyint(2) NOT NULL,
  `access_token` varchar(512) NOT NULL,
  `expires_date` int(11) NOT NULL COMMENT 'access_token过期时间',
  `openid` varchar(40) NOT NULL COMMENT '第三方用户id',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='第三方用户表';

-- ----------------------------
-- Records of cmf_oauth_user
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_options
-- ----------------------------
DROP TABLE IF EXISTS `cmf_options`;
CREATE TABLE `cmf_options` (
  `option_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `option_name` varchar(64) NOT NULL COMMENT '配置名',
  `option_value` longtext NOT NULL COMMENT '配置值',
  `autoload` int(2) NOT NULL DEFAULT '1' COMMENT '是否自动加载',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=utf8 COMMENT='全站配置表';

-- ----------------------------
-- Records of cmf_options
-- ----------------------------
INSERT INTO `cmf_options` VALUES ('1', 'member_email_active', '{\"title\":\"\\u90ae\\u4ef6\\u6fc0\\u6d3b\\u901a\\u77e5.\",\"template\":\"<p><br\\/>&nbsp; &nbsp;<strong>---------------<\\/strong><br\\/>&nbsp; &nbsp;<strong>\\u5e10\\u53f7\\u6fc0\\u6d3b\\u8bf4\\u660e<\\/strong><br\\/>&nbsp; &nbsp;<strong>---------------<\\/strong><br\\/><br\\/>&nbsp; &nbsp; \\u5c0a\\u656c\\u7684<span style=\\\"FONT-SIZE: 16px; FONT-FAMILY: Arial; COLOR: rgb(51,51,51); LINE-HEIGHT: 18px; BACKGROUND-COLOR: rgb(255,255,255)\\\">#username#\\uff0c\\u60a8\\u597d\\u3002<\\/span>\\u5982\\u679c\\u60a8\\u662fThinkCMF\\u7684\\u65b0\\u7528\\u6237\\uff0c\\u6216\\u5728\\u4fee\\u6539\\u60a8\\u7684\\u6ce8\\u518cEmail\\u65f6\\u4f7f\\u7528\\u4e86\\u672c\\u5730\\u5740\\uff0c\\u6211\\u4eec\\u9700\\u8981\\u5bf9\\u60a8\\u7684\\u5730\\u5740\\u6709\\u6548\\u6027\\u8fdb\\u884c\\u9a8c\\u8bc1\\u4ee5\\u907f\\u514d\\u5783\\u573e\\u90ae\\u4ef6\\u6216\\u5730\\u5740\\u88ab\\u6ee5\\u7528\\u3002<br\\/>&nbsp; &nbsp; \\u60a8\\u53ea\\u9700\\u70b9\\u51fb\\u4e0b\\u9762\\u7684\\u94fe\\u63a5\\u5373\\u53ef\\u6fc0\\u6d3b\\u60a8\\u7684\\u5e10\\u53f7\\uff1a<br\\/>&nbsp; &nbsp; <a title=\\\"\\\" href=\\\"http:\\/\\/#link#\\\" target=\\\"_self\\\">http:\\/\\/#link#<\\/a><br\\/>&nbsp; &nbsp; (\\u5982\\u679c\\u4e0a\\u9762\\u4e0d\\u662f\\u94fe\\u63a5\\u5f62\\u5f0f\\uff0c\\u8bf7\\u5c06\\u8be5\\u5730\\u5740\\u624b\\u5de5\\u7c98\\u8d34\\u5230\\u6d4f\\u89c8\\u5668\\u5730\\u5740\\u680f\\u518d\\u8bbf\\u95ee)<br\\/>&nbsp; &nbsp; \\u611f\\u8c22\\u60a8\\u7684\\u8bbf\\u95ee\\uff0c\\u795d\\u60a8\\u4f7f\\u7528\\u6109\\u5feb\\uff01<br\\/><br\\/>&nbsp; &nbsp; \\u6b64\\u81f4<br\\/>&nbsp; &nbsp; \\u6b63\\u5348\\u9633\\u5149 \\u7ba1\\u7406\\u56e2\\u961f.<\\/p>\"}', '1');
INSERT INTO `cmf_options` VALUES ('3', 'cmf_settings', '{\"banned_usernames\":\"\"}', '1');
INSERT INTO `cmf_options` VALUES ('2', 'site_options', '{\"site_name\":\"\\u6b63\\u5348\\u9633\\u5149\",\"site_host\":\"http:\\/\\/1805595e.all123.net\\/\",\"site_admin_url_password\":\"\",\"site_tpl\":\"simplebootx\",\"site_adminstyle\":\"bluesky\",\"site_icp\":\"\\u4eacICP\\u590713015845\\u53f7-1\",\"site_admin_email\":\"MAIL@DAYLIGHT.COM\",\"site_tongji\":\"\",\"site_copyright\":\"Copyright (c) 2016 daylight.com\",\"site_seo_title\":\"\\u6b63\\u5348\\u9633\\u5149\",\"site_seo_keywords\":\"\\u6b63\\u5348\\u9633\\u5149\",\"site_seo_description\":\"\\u6b63\\u5348\\u9633\\u5149\",\"urlmode\":\"0\",\"html_suffix\":\"\",\"comment_time_interval\":60}', '1');

-- ----------------------------
-- Table structure for cmf_plugins
-- ----------------------------
DROP TABLE IF EXISTS `cmf_plugins`;
CREATE TABLE `cmf_plugins` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT COMMENT '自增id',
  `name` varchar(50) NOT NULL COMMENT '插件名，英文',
  `title` varchar(50) NOT NULL DEFAULT '' COMMENT '插件名称',
  `description` text COMMENT '插件描述',
  `type` tinyint(2) DEFAULT '0' COMMENT '插件类型, 1:网站；8;微信',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态；1开启；',
  `config` text COMMENT '插件配置',
  `hooks` varchar(255) DEFAULT NULL COMMENT '实现的钩子;以“，”分隔',
  `has_admin` tinyint(2) DEFAULT '0' COMMENT '插件是否有后台管理界面',
  `author` varchar(50) DEFAULT '' COMMENT '插件作者',
  `version` varchar(20) DEFAULT '' COMMENT '插件版本号',
  `createtime` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '插件安装时间',
  `listorder` smallint(6) NOT NULL DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='插件表';

-- ----------------------------
-- Records of cmf_plugins
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_posts
-- ----------------------------
DROP TABLE IF EXISTS `cmf_posts`;
CREATE TABLE `cmf_posts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned DEFAULT '0' COMMENT '发表者id',
  `post_keywords` varchar(150) NOT NULL COMMENT 'seo keywords',
  `post_source` varchar(150) DEFAULT NULL COMMENT '转载文章的来源',
  `post_date` datetime DEFAULT '2000-01-01 00:00:00' COMMENT 'post创建日期，永久不变，一般不显示给用户',
  `post_content` longtext COMMENT 'post内容',
  `post_title` text COMMENT 'post标题',
  `post_excerpt` text COMMENT 'post摘要',
  `post_status` int(2) DEFAULT '1' COMMENT 'post状态，1已审核，0未审核',
  `comment_status` int(2) DEFAULT '1' COMMENT '评论状态，1允许，0不允许',
  `post_modified` datetime DEFAULT '2000-01-01 00:00:00' COMMENT 'post更新时间，可在前台修改，显示给用户',
  `post_content_filtered` longtext,
  `post_parent` bigint(20) unsigned DEFAULT '0' COMMENT 'post的父级post id,表示post层级关系',
  `post_type` int(2) DEFAULT NULL,
  `post_mime_type` varchar(100) DEFAULT '',
  `comment_count` bigint(20) DEFAULT '0',
  `smeta` text COMMENT 'post的扩展字段，保存相关扩展属性，如缩略图；格式为json',
  `post_hits` int(11) DEFAULT '0' COMMENT 'post点击数，查看数',
  `post_like` int(11) DEFAULT '0' COMMENT 'post赞数',
  `istop` tinyint(1) NOT NULL DEFAULT '0' COMMENT '置顶 1置顶； 0不置顶',
  `recommended` tinyint(1) NOT NULL DEFAULT '0' COMMENT '推荐 1推荐 0不推荐',
  PRIMARY KEY (`id`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`id`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`),
  KEY `post_date` (`post_date`) USING BTREE
) ENGINE=MyISAM AUTO_INCREMENT=23 DEFAULT CHARSET=utf8 COMMENT='Portal文章表';

-- ----------------------------
-- Records of cmf_posts
-- ----------------------------
INSERT INTO `cmf_posts` VALUES ('1', '1', 'testasfasd', '', '2016-04-02 14:46:09', '<p>华鼎奖“最佳电视剧男演员”之争呈现白热化状态，胡歌与霍建华同时入围。近年来胡歌与霍建华花开两朵各表一枝，这边胡歌《琅琊榜》《伪装者》帅出一脸血，那边霍建华《花千骨》《他来了，请闭眼》潇洒引“舔屏”，当“胡霍之争”邂逅华鼎奖，一场激烈争夺战一触即发。参与最佳电视剧男演员竞争的还有新晋小生王凯，他与“胡霍”之间也有着千丝万缕的联系，在《琅琊榜》与《他来了，请闭眼》中的精彩表现同样可圈可点。另外还有李晨和陆毅，“范氏姑爷”李晨过年不忘秀恩爱，跟范冰冰回老家疑好事将近，其近年来除了在“跑男”以“大黑牛”身份备受瞩目之外，更是以《武媚娘传奇》《三个奶爸》等高收视电视剧热受追捧，更有前任张馨予与现任范冰冰“撕逼大战”中的绅士表现成功晋升正面话题人物。“长腿爸爸”陆毅低调奢华有内涵，凭借《秦时明月》与《云中歌》成功洗去奶油味儿转型“魅力帅叔”系列，与鲍蕾结合生下两女组“一家子大长腿”，暖男正能量满满当当，能否在本届华鼎奖上有所斩获也让人关注。</p>', '第18届华鼎奖盛典 胡歌霍建华王凯争视帝', '第18届华鼎奖全球演艺名人满意度调查发布盛典将于3月31日在澳门举行，昨天组委会公布了提名名单...', '1', '1', '2016-04-02 14:45:23', null, '0', null, '', '0', '{\"thumb\":\"\",\"photo\":[{\"url\":\"20160402\\/56ffa12a96aa5.png\",\"alt\":\"news-pic\"}]}', '82', '0', '1', '1');
INSERT INTO `cmf_posts` VALUES ('2', '1', '公司介绍', null, '2016-04-03 10:32:02', '<p>东阳正午阳光影视有限公司（简称“正午阳光影业”）成立于2011年，是一家具有专业水准的综合性影视机构，集影视策划制作发行、特效技术与后期制作、娱乐营销、演艺经纪为一体，拥有一支以制片人侯鸿亮、导演孔笙、导演李雪、导演孙墨龙为创作主体的国内顶级制作团队。</p><p>这支团队在一起搭档20年，以提供真正意义的精品内容为己任，作品获遍中国各国家级电视剧类奖项，包括11次“飞天”奖、5次“金鹰”奖、4次“五个一工程”奖、3次“白玉兰”奖。这其中，囊括了各题材领域的标杆，如作为移民剧标杆的《闯关东》，作为战争剧标杆的《生死线》，作为当代剧标杆的《温州一家人》，作为家庭剧标杆的《父母爱情》，作为年代剧标杆的《北平无战事》，作为古装传奇剧标杆的《琅琊榜》。</p><p>正午阳光影业，便是由这支代表着国剧最高制作水准的团队于2011年8月创立，初期以制作为主，承担了多部戏的后期工作及《父母爱情》、《北平无战事》的摄制。2014年10月起进行改组，建立健全策划、制作、发行、经纪、商务等部门，突出公司主营业务，策划制作出品发行了《琅琊榜》、《伪装者》、《温州两家人》、《他来了，请闭眼》、《欢乐颂》等剧。</p><p>其中已播出的《北平无战事》、《伪装者》、《琅琊榜》，不仅电视收视和网络播放均取得了出色成绩，更在2014-2015年的中国电视剧市场，引发了“现象级“的轰动效应，全民观剧蔚然成风。《北平无战事》因其在剧本、制作、表演、文学、史学价值及人文精神等方面的极致追求和一流表达，为市场生态严重失衡，同质化、反智向风行的中国荧屏，赢回了大量有知识、有分量、有市场价值的观众，在影视、文艺、金融、史学界和广大电视观众群中均引起热烈的反响和积极的回应。不仅仅是刷新了中国电视剧高度，其深远意义更是关系到中国电视剧在艺术性上的正名；《伪装者》的播出，则给2015低迷的中国电视剧市场带来了久违的收视兴奋和话题热度，更是为亟待破局的谍战剧市场打开了一个风口，既是一次谍战剧向偶像剧的借鉴或转化，也是两个类型的亲密结合，这可以说是荧屏剧，甚至整个华语影视圈的转变方向之一；《琅琊榜》在中国更是引发了近年罕见的全民热议的追剧盛况，剧情和人物的方方面面、覆盖到制作的各个环节的用心之处、几乎是出场的每一位演员以及价值观的明亮坚持，都成为观众津津乐道的热点，被评价为有瑰丽的想象，也有飘逸的情致，更有扎实厚重的格调，拥有独特的审美风格和精致品相，即使剧集落幕也未曾阻挡各种话题的发酵，受到了各界的关注、跟风，各行各业都在从不同的角度切入，巧妙地将《琅琊榜》话题植入到自身选题定位当中，热度一路高歌挺进。</p><p>在依靠以上优质内容塑造品牌的同时，正午阳光影业也在不断加强创作人才的积累，整合行业优势资源，不断在题材内容、运作模式上进行创新，致力于做各个屏的尖端产品，用真正的好内容去征服市场，做无可替代的精品，独树一帜的品牌，打造真正有文化影响力的作品。</p><p><br/></p>', '公司介绍', '公司介绍', '1', '1', '2016-04-03 10:31:32', null, '0', '2', '', '0', '{\"template\":\"aboutus\",\"thumb\":\"20160403\\/570083165c0cc.jpg\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('3', '1', '明星档案', null, '2016-04-03 11:16:18', '<p>明星档案</p>', '明星档案', '明星档案', '1', '1', '2016-04-03 11:15:30', null, '0', '2', '', '0', '{\"template\":\"starslist\",\"thumb\":\"\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('4', '1', '个人档案', null, '2016-04-03 12:07:44', '<p>个人档案</p>', '个人档案', '个人档案', '1', '1', '2016-04-03 12:06:49', null, '0', '2', '', '0', '{\"template\":\"star\",\"thumb\":\"\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('5', '1', '郭涛个人档案', '', '2016-04-03 12:35:42', '<div class=\"star-photo-con\">\r\n    <img src=\"/sunshine/data/upload/ueditor/20160404/5701e5e497fc7.png\" alt=\"57009028899f9.png\" title=\"57009028899f9.png\"/>\r\n</div>\r\n<div class=\"star-info-con\">\r\n    <div class=\"star-info-CNname\">\r\n        郭涛\r\n    </div>\r\n    <div class=\"star-info-Enname\">\r\n        Guo Tao\r\n    </div>\r\n    <div class=\"star-Constellation\">\r\n        <span class=\"starinfo-left\">星座</span><span class=\"starinfo-right\">狮子座</span>\r\n    </div>\r\n    <div class=\"star-blood\">\r\n        <span class=\"starinfo-left\">血型</span><span class=\"starinfo-right\">A </span>\r\n    </div>\r\n    <div class=\"star-height\">\r\n        <span class=\"starinfo-left\">身高</span><span class=\"starinfo-right\">182cm </span>\r\n    </div>\r\n    <div class=\"star-weight\">\r\n        <span class=\"starinfo-left\">体重</span><span class=\"starinfo-right\">70kg</span>\r\n    </div>\r\n    <div class=\"star-addr\">\r\n        <span class=\"starinfo-left\">出生地</span><span class=\"starinfo-right\">湖北武汉 </span>\r\n    </div>\r\n    <div class=\"birth\">\r\n        <span class=\"starinfo-left\">生日</span><span class=\"starinfo-right\">1982年8月18日（农历六月二十九）</span>\r\n    </div>\r\n    <div class=\"star-colleage\">\r\n        <span class=\"starinfo-left\">毕业院校</span><span class=\"starinfo-right\">中央戏剧学院03级表演本科</span>\r\n    </div>\r\n    <div class=\"star-works\">\r\n        <span class=\"starinfo-left\">代表作品</span><span class=\"starinfo-right\">新神探联盟、北平无战事、伪装者、琅琊榜 </span>\r\n    </div>\r\n    <div class=\"star-awards\">\r\n        <div class=\"starinfo-left\">\r\n            主要成就\r\n        </div>\r\n        <div class=\"starinfo-right\">\r\n            “风从东方来”娱乐影响力盛典“年度最具人气电视剧演员”奖<br/>\r\n				 2015国剧盛典内地最具人气演员<br/>\r\n				 2015国剧盛典年度演技飞跃演员<br/>\r\n        </div>\r\n    </div>\r\n    <div class=\"star-shop-con\">\r\n        <img class=\"cursor-pointer\" src=\"/sunshine/themes/simplebootx/Public/statics/style/images/star-shop.png\"/>\r\n    </div>\r\n</div>', '郭涛', '郭涛个人档案', '1', '1', '2016-04-03 12:35:22', null, '0', null, '', '0', '{\"thumb\":\"20160515\\/57386da847e94.png\",\"photo\":[{\"url\":\"20160404\\/5701dfd08a11e.png\",\"alt\":\"5700b8d5dcdc4\"}]}', '242', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('6', '1', '刘奕君', '', '2016-04-03 14:31:59', '<p>刘奕君</p>', '刘奕君', '刘奕君', '1', '1', '2016-04-03 14:31:30', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/5700ba7914d48.png\"}', '7', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('7', '1', '', '', '2016-04-03 15:07:21', '<p><img src=\"__TMPL__Public/statics/style/images/star-photo.png\" alt=\"\"/></p><p>王凯</p><p>Wang Kai</p><p><span class=\"starinfo-left\">星座</span><span class=\"starinfo-right\">狮子座</span></p><p><span class=\"starinfo-left\">血型</span><span class=\"starinfo-right\">A </span></p><p><span class=\"starinfo-left\">身高</span><span class=\"starinfo-right\">182cm </span></p><p><span class=\"starinfo-left\">体重</span><span class=\"starinfo-right\">70kg</span></p><p><span class=\"starinfo-left\">出生地</span><span class=\"starinfo-right\">湖北武汉 </span></p><p><span class=\"starinfo-left\">生日</span><span class=\"starinfo-right\">1982年8月18日（农历六月二十九）</span></p><p><span class=\"starinfo-left\">毕业院校</span><span class=\"starinfo-right\">中央戏剧学院03级表演本科</span></p><p><span class=\"starinfo-left\">代表作品</span><span class=\"starinfo-right\">新神探联盟、北平无战事、伪装者、琅琊榜 </span></p><p>主要成就</p><p>“风从东方来”娱乐影响力盛典“年度最具人气电视剧演员”奖<br/>\r\n &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2015国剧盛典内地最具人气演员<br/>\r\n &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; 2015国剧盛典年度演技飞跃演员<br/></p><p><img class=\"cursor-pointer\" src=\"__TMPL__Public/statics/style/images/star-shop.png\"/></p>', '赵达', '赵达', '1', '1', '2016-04-03 15:05:34', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/5700c0d096492.png\"}', '4', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('8', '1', '', '', '2016-04-03 15:09:24', '<h1 class=\"ue_t\" label=\"Title center\" name=\"tc\" style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;text-align:center;margin:0px 0px 20px;\"><span style=\"color:#c0504d;\">[键入文档标题]</span></h1><p style=\"text-align:center;\"><strong class=\"ue_t\">[键入文档副标题]</strong></p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 1]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 2]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">在“开始”选项卡上，通过从快速样式库中为所选文本选择一种外观，您可以方便地更改文档中所选文本的格式。 您还可以使用“开始”选项卡上的其他控件来直接设置文本格式。大多数控件都允许您选择是使用当前主题外观，还是使用某种直接指定的格式。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 3]</span></h3><p class=\"ue_t\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><p class=\"ue_t\"><br/></p><p><br/></p>', '岳阳', '岳阳', '1', '1', '2016-04-03 15:08:34', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/5700c187967a1.png\"}', '2', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('9', '1', '', '', '2016-04-03 15:10:13', '<h1 class=\"ue_t\" label=\"Title center\" name=\"tc\" style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;text-align:center;margin:0px 0px 20px;\"><span style=\"color:#c0504d;\">[键入文档标题]</span></h1><p style=\"text-align:center;\"><strong class=\"ue_t\">[键入文档副标题]</strong></p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 1]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 2]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">在“开始”选项卡上，通过从快速样式库中为所选文本选择一种外观，您可以方便地更改文档中所选文本的格式。 您还可以使用“开始”选项卡上的其他控件来直接设置文本格式。大多数控件都允许您选择是使用当前主题外观，还是使用某种直接指定的格式。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 3]</span></h3><p class=\"ue_t\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><p class=\"ue_t\"><br/></p><p><br/></p>', '靳东', '靳东', '1', '1', '2016-04-03 15:09:26', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/5700c1c50afd8.png\"}', '5', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('10', '1', '', '', '2016-04-03 15:11:01', '<h1 class=\"ue_t\" label=\"Title center\" name=\"tc\" style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;text-align:center;margin:0px 0px 20px;\"><span style=\"color:#c0504d;\">[键入文档标题]</span></h1><p style=\"text-align:center;\"><strong class=\"ue_t\">[键入文档副标题]</strong></p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 1]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 2]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">在“开始”选项卡上，通过从快速样式库中为所选文本选择一种外观，您可以方便地更改文档中所选文本的格式。 您还可以使用“开始”选项卡上的其他控件来直接设置文本格式。大多数控件都允许您选择是使用当前主题外观，还是使用某种直接指定的格式。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 3]</span></h3><p class=\"ue_t\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><p class=\"ue_t\"><br/></p><p><br/></p>', '郭晓然', '郭晓然', '1', '1', '2016-04-03 15:10:14', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/5700c1f22b1d8.png\"}', '3', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('11', '1', '', '', '2016-04-03 15:11:33', '<h1 class=\"ue_t\" label=\"Title center\" name=\"tc\" style=\"border-bottom-color:#cccccc;border-bottom-width:2px;border-bottom-style:solid;padding:0px 4px 0px 0px;text-align:center;margin:0px 0px 20px;\"><span style=\"color:#c0504d;\">[键入文档标题]</span></h1><p style=\"text-align:center;\"><strong class=\"ue_t\">[键入文档副标题]</strong></p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 1]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 2]</span></h3><p class=\"ue_t\" style=\"text-indent:2em;\">在“开始”选项卡上，通过从快速样式库中为所选文本选择一种外观，您可以方便地更改文档中所选文本的格式。 您还可以使用“开始”选项卡上的其他控件来直接设置文本格式。大多数控件都允许您选择是使用当前主题外观，还是使用某种直接指定的格式。</p><h3><span class=\"ue_t\" style=\"font-family:幼圆\">[标题 3]</span></h3><p class=\"ue_t\">对于“插入”选项卡上的库，在设计时都充分考虑了其中的项与文档整体外观的协调性。 您可以使用这些库来插入表格、页眉、页脚、列表、封面以及其他文档构建基块。 您创建的图片、图表或关系图也将与当前的文档外观协调一致。</p><p class=\"ue_t\"><br/></p><p><br/></p>', '张陆', '张陆', '1', '1', '2016-04-03 15:11:04', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/5700c216b6a84.png\"}', '3', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('12', '1', '', '', '2016-04-03 23:26:30', '<p><span style=\"color: rgb(102, 102, 102); font-family: &#39;microsoft yahei&#39;, sans-serif; font-size: 13px; letter-spacing: 2px; line-height: 32px; text-indent: 26px; background-color: rgb(246, 246, 246);\">华鼎奖“最佳电视剧男演员”之争呈现白热化状态，胡歌与霍建华同时入围。近年来胡歌与霍建华花开两朵各表一枝，这边胡歌《琅琊榜》《伪装者》帅出一脸血，那边霍建华《花千骨》《他来了，请闭眼》潇洒引“舔屏”，当“胡霍之争”邂逅华鼎奖，一场激烈争夺战一触即发。参与最佳电视剧男演员竞争的还有新晋小生王凯，他与“胡霍”之间也有着千丝万缕的联系，在《琅琊榜》与《他来了，请闭眼》中的精彩表现同样可圈可点。另外还有李晨和陆毅，“范氏姑爷”李晨过年不忘秀恩爱，跟范冰冰回老家疑好事将近，其近年来除了在“跑男”以“大黑牛”身份备受瞩目之外，更是以《武媚娘传奇》《三个奶爸》等高收视电视剧热受追捧，更有前任张馨予与现任范冰冰“撕逼大战”中的绅士表现成功晋升正面话题人物。“长腿爸爸”陆毅低调奢华有内涵，凭借《秦时明月》与《云中歌》成功洗去奶油味儿转型“魅力帅叔”系列，与鲍蕾结合生下两女组“一家子大长腿”，暖男正能量满满当当，能否在本届华鼎奖上有所斩获也让人关注。</span></p>', '郭涛来袭', '郭涛来袭', '1', '1', '2016-04-03 23:25:43', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/57013621d9594.png\",\"photo\":[{\"url\":\"20160403\\/570136d2180e7.png\",\"alt\":\"57009028899f9\"}]}', '12', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('13', '1', '', '', '2016-04-03 23:33:46', '<p><span style=\"color: rgb(51, 51, 51); font-family: &#39;microsoft yahei&#39;, sans-serif; font-size: 14px; letter-spacing: 1.3px; line-height: 34px; background-color: rgb(255, 255, 255);\">第18届华鼎奖盛典 胡歌霍建华王凯争视帝</span></p>', '第18届华鼎奖盛典 胡歌霍建华王凯争视帝', '第18届华鼎奖盛典 胡歌霍建华王凯争视帝', '1', '1', '2016-04-03 23:33:17', null, '0', null, '', '0', '{\"thumb\":\"\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('14', '1', '', '', '2016-04-03 23:33:58', '<p><span style=\"color: rgb(51, 51, 51); font-family: &#39;microsoft yahei&#39;, sans-serif; font-size: 14px; letter-spacing: 1.3px; line-height: 34px; background-color: rgb(255, 255, 255);\">第18届华鼎奖盛典 胡歌霍建华王凯争视帝</span></p>', '第18届华鼎奖盛典 胡歌霍建华王凯争视帝', '第18届华鼎奖盛典 胡歌霍建华王凯争视帝', '1', '1', '2016-04-03 23:33:48', null, '0', null, '', '0', '{\"thumb\":\"\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('15', '1', '', '', '2016-04-03 23:47:09', '<p>相关影像</p>', '【图集】王凯相关影像', '【图集】王凯相关影像', '1', '1', '2016-04-03 23:45:45', null, '0', null, '', '0', '{\"thumb\":\"20160403\\/57013b6de7b1d.png\",\"photo\":[{\"url\":\"20160403\\/57013acc4afa2.png\",\"alt\":\"57009028899f9\"},{\"url\":\"20160403\\/57013ad2050eb.png\",\"alt\":\"5700915878607\"},{\"url\":\"20160403\\/57013b76d52d6.png\",\"alt\":\"5700921ff0eea\"}]}', '1', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('16', '1', '', null, '2016-05-15 10:43:51', '<p>影视剧集</p>', '影视剧集', '影视剧集', '1', '1', '2016-05-15 10:43:15', null, '0', '2', '', '0', '{\"template\":\"videos\",\"thumb\":\"\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('17', '1', '', '', '2016-05-15 13:08:14', '<div class=\"desc span_3_of_2\"><h2>琅琊榜</h2><div class=\"available\"><ul class=\" list-paddingleft-2\"><li><p><span class=\"z-label\">集数：</span>&nbsp; 533333</p></li><li><p><span class=\"z-label\">题材：</span>&nbsp; 古装传奇</p></li><li><p><span class=\"z-label\">编剧：</span>&nbsp; 海晏</p></li><li><p><span class=\"z-label\">导演：</span>&nbsp; 孔笙 李雪</p></li><li><p><span class=\"z-label\">制片人：&nbsp;</span>侯洪亮</p></li><li><p><span class=\"z-label\">主演：</span>&nbsp; 胡歌、刘涛、王凯、黄维德、陈龙、刘敏涛、靳东、高鑫、刘奕君、昊磊、张玲心、王鸥、张琰琰</p></li><li><p><span class=\"z-label\">首播：</span>&nbsp; 北京卫视 东方卫视</p></li><li><p><span class=\"z-label\">首播时间：</span>&nbsp; 2015年8月</p></li></ul></div><div class=\"wish-list\"><ul class=\" list-paddingleft-2\"><span style=\"margin-right: 20px;\">在线观看:</span><li><p class=\"icon-play-circle\"><a href=\"#\" target=\"_self\" textvalue=\"优酷\">优酷</a></p></li><li><p class=\"icon-play-circle\"><a href=\"#\">爱奇艺</a></p></li><li><p class=\"icon-play-circle\"><a href=\"#\">乐视视频</a></p></li></ul></div></div><p><br/></p>', '琅琊榜剧情基本信息', '琅琊榜剧情基本信息', '1', '1', '2016-05-15 13:03:52', null, '0', null, '', '0', '{\"thumb\":\"20160515\\/5738042c43757.jpg\"}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('18', '1', '', '', '2016-05-15 13:29:00', '<p>华鼎奖“最佳电视剧男演员”之争呈现白热化状态，胡歌与霍建华同时入围。近年来胡歌与霍建华花开两朵各表一枝，这边胡歌《琅琊榜》《伪装者》帅出一脸血，那边霍建华《花千骨》《他来了，请闭眼》潇洒引“舔屏”，当“胡霍之争”邂逅华鼎奖，一场激烈争夺战一触即发。参与最佳电视剧男演员竞争的还有新晋小生王凯，他与“胡霍”之间也有着千丝万缕的联系，在《琅琊榜》与《他来了，请闭眼》中的精彩表现同样可圈可点。另外还有李晨和陆毅，“范氏姑爷”李晨过年不忘秀恩爱，跟范冰冰回老家疑好事将近，其近年来除了在“跑男”以“大黑牛”身份备受瞩目之外，更是以《武媚娘传奇》《三个奶爸》等高收视电视剧热受追捧，更有前任张馨予与现任范冰冰“撕逼大战”中的绅士表现成功晋升正面话题人物。“长腿爸爸”陆毅低调奢华有内涵，凭借《秦时明月》与《云中歌》成功洗去奶油味儿转型“魅力帅叔”系列，与鲍蕾结合生下两女组“一家子大长腿”，暖男正能量满满当当，能否在本届华鼎奖上有所斩获也让人关注。</p>', '琅琊榜简介', '琅琊榜简介琅琊榜简介琅琊榜简介', '1', '1', '2016-05-15 13:27:29', null, '0', null, '', '0', '{\"thumb\":\"\",\"photo\":[{\"url\":\"20160515\\/5738090da190c.png\",\"alt\":\"56ffa12a96aa5\"},{\"url\":\"20160515\\/57380d2293593.jpg\",\"alt\":\"slider-pic\"}]}', '0', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('19', '1', '', '', '2016-05-15 13:55:47', '<p>琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮琅琊榜拍摄花絮</p>', '琅琊榜拍摄花絮', '琅琊榜拍摄花絮', '1', '1', '2016-05-15 13:55:14', null, '0', null, '', '0', '{\"thumb\":\"\"}', '5', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('20', '1', '', '', '2016-05-15 13:59:04', '<p>琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2琅琊榜拍摄花絮2</p>', '琅琊榜拍摄花絮2', '琅琊榜拍摄花絮2', '1', '1', '2016-05-15 13:58:31', null, '0', null, '', '0', '{\"thumb\":\"\",\"photo\":[{\"url\":\"20160515\\/57381023cbbe8.jpg\",\"alt\":\"slider-pic\"}]}', '1', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('21', '1', '', '', '2016-05-15 14:00:17', '<p>琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻琅琊榜相关新闻</p>', '琅琊榜相关新闻', '琅琊榜相关新闻琅琊榜相关新闻', '1', '1', '2016-05-15 13:59:05', null, '0', null, '', '0', '{\"thumb\":\"\",\"photo\":[{\"url\":\"20160515\\/5738106db41e3.jpg\",\"alt\":\"Hydrangeas\"}]}', '2', '0', '0', '0');
INSERT INTO `cmf_posts` VALUES ('22', '1', '', '', '2016-05-15 16:44:48', '<p>\r\n    <video class=\"edui-upload-video  vjs-default-skin video-js\" controls=\"\" preload=\"none\" width=\"420\" height=\"280\" src=\"http://localhost/sunshine/data/upload/ueditor/20160515/57383685f34d1.mp4\" data-setup=\"{}\">\r\n        <source src=\"http://localhost/sunshine/data/upload/ueditor/20160515/57383685f34d1.mp4\" type=\"video/mp4\"/>\r\n    </video>\r\n</p>', 'test-video', 'test-video', '1', '1', '2016-05-15 16:39:20', null, '0', null, '', '0', '{\"thumb\":\"20160515\\/573836e35184b.jpg\"}', '0', '0', '0', '0');

-- ----------------------------
-- Table structure for cmf_role
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role`;
CREATE TABLE `cmf_role` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL COMMENT '角色名称',
  `pid` smallint(6) DEFAULT NULL COMMENT '父角色ID',
  `status` tinyint(1) unsigned DEFAULT NULL COMMENT '状态',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `create_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '创建时间',
  `update_time` int(11) unsigned NOT NULL DEFAULT '0' COMMENT '更新时间',
  `listorder` int(3) NOT NULL DEFAULT '0' COMMENT '排序字段',
  PRIMARY KEY (`id`),
  KEY `parentId` (`pid`),
  KEY `status` (`status`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of cmf_role
-- ----------------------------
INSERT INTO `cmf_role` VALUES ('1', '超级管理员', '0', '1', '拥有网站最高管理员权限！', '1329633709', '1329633709', '0');

-- ----------------------------
-- Table structure for cmf_role_user
-- ----------------------------
DROP TABLE IF EXISTS `cmf_role_user`;
CREATE TABLE `cmf_role_user` (
  `role_id` int(11) unsigned DEFAULT '0' COMMENT '角色 id',
  `user_id` int(11) DEFAULT '0' COMMENT '用户id',
  KEY `group_id` (`role_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户角色对应表';

-- ----------------------------
-- Records of cmf_role_user
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_route
-- ----------------------------
DROP TABLE IF EXISTS `cmf_route`;
CREATE TABLE `cmf_route` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '路由id',
  `full_url` varchar(255) DEFAULT NULL COMMENT '完整url， 如：portal/list/index?id=1',
  `url` varchar(255) DEFAULT NULL COMMENT '实际显示的url',
  `listorder` int(5) DEFAULT '0' COMMENT '排序，优先级，越小优先级越高',
  `status` tinyint(1) DEFAULT '1' COMMENT '状态，1：启用 ;0：不启用',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='url路由表';

-- ----------------------------
-- Records of cmf_route
-- ----------------------------

-- ----------------------------
-- Table structure for cmf_slide
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide`;
CREATE TABLE `cmf_slide` (
  `slide_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slide_cid` int(11) NOT NULL COMMENT '幻灯片分类 id',
  `slide_name` varchar(255) NOT NULL COMMENT '幻灯片名称',
  `slide_pic` varchar(255) DEFAULT NULL COMMENT '幻灯片图片',
  `slide_url` varchar(255) DEFAULT NULL COMMENT '幻灯片链接',
  `slide_des` varchar(255) DEFAULT NULL COMMENT '幻灯片描述',
  `slide_content` text COMMENT '幻灯片内容',
  `slide_status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  `listorder` int(10) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`slide_id`),
  KEY `slide_cid` (`slide_cid`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='幻灯片表';

-- ----------------------------
-- Records of cmf_slide
-- ----------------------------
INSERT INTO `cmf_slide` VALUES ('1', '1', '首页banner图集', '/sunshine/data/upload/20160514/573691389df40.jpg', '', '第一页第一页', '第一页第一页第一页第一页第一页第一页', '1', '0');
INSERT INTO `cmf_slide` VALUES ('2', '1', '首页banner图集', '/sunshine/data/upload/20160402/56ff5833d40c3.jpg', '', '第二页第二页', '第二页第二页第二页第二页第二页第二页', '1', '0');
INSERT INTO `cmf_slide` VALUES ('3', '2', '公司介绍', '/sunshine/data/upload/20160402/56ff5a75bba59.jpg', '', '公司介绍公司介绍', '公司介绍公司介绍公司介绍公司介绍', '1', '0');
INSERT INTO `cmf_slide` VALUES ('4', '2', '公司介绍', '/sunshine/data/upload/20160402/56ff5a83aee6e.jpg', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('5', '2', '公司介绍', '/sunshine/data/upload/20160402/56ff5a8dc5810.jpg', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('6', '3', '影视剧目', '/sunshine/data/upload/20160402/56ff5e79abf8b.jpg', '', '影视剧目影视剧目影视剧目', '影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目', '1', '0');
INSERT INTO `cmf_slide` VALUES ('7', '3', '影视剧目', '/sunshine/data/upload/20160402/56ff5e9042546.jpg', '', '影视剧目影视剧目影视剧目', '影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目', '1', '0');
INSERT INTO `cmf_slide` VALUES ('8', '3', '影视剧目', '/sunshine/data/upload/20160402/56ff5ea2a4f9b.jpg', '', '影视剧目影视剧目影视剧目', '影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目', '1', '0');
INSERT INTO `cmf_slide` VALUES ('9', '3', '影视剧目', '/sunshine/data/upload/20160402/56ff5eb361d37.jpg', '', '影视剧目影视剧目影视剧目', '影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目影视剧目', '1', '0');
INSERT INTO `cmf_slide` VALUES ('10', '4', '马丁·劳伦斯', '/sunshine/data/upload/20160402/56ff631219cd1.jpg', '', 'Martin Laurence', '代表作品：《超级奶爸》《绝地战警1》《绝地战警2》', '1', '0');
INSERT INTO `cmf_slide` VALUES ('11', '4', '靳冬', '/sunshine/data/upload/20160402/56ff63690c1f6.jpg', '', 'Jin Dong', '代表作品：日出、秋雨、伪装者、箭在弦上、温州一家人、闯关东', '1', '0');
INSERT INTO `cmf_slide` VALUES ('12', '5', '郭涛', '/sunshine/data/upload/20160403/57009028899f9.png', '', 'guotao', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('13', '5', '刘奕君', '/sunshine/data/upload/20160403/5700911843f72.png', '', 'liuyijun', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('14', '5', '赵达', '/sunshine/data/upload/20160403/57009137087c1.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('15', '5', '岳阳', '/sunshine/data/upload/20160403/5700915878607.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('16', '5', '靳东', '/sunshine/data/upload/20160403/57009174e7c74.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('17', '5', '郭晓然', '/sunshine/data/upload/20160403/57009186a6317.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('18', '5', '张陆', '/sunshine/data/upload/20160403/57009198e38b2.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('19', '6', '张琰琰', '/sunshine/data/upload/20160403/570092100bdd6.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('20', '6', '乔欣', '/sunshine/data/upload/20160403/5700921ff0eea.png', '', '', '', '1', '0');
INSERT INTO `cmf_slide` VALUES ('21', '6', '刘敏涛', '/sunshine/data/upload/20160403/5700922fe6046.png', '', '', '', '1', '0');

-- ----------------------------
-- Table structure for cmf_slide_cat
-- ----------------------------
DROP TABLE IF EXISTS `cmf_slide_cat`;
CREATE TABLE `cmf_slide_cat` (
  `cid` int(11) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(255) NOT NULL COMMENT '幻灯片分类',
  `cat_idname` varchar(255) NOT NULL COMMENT '幻灯片分类标识',
  `cat_remark` text COMMENT '分类备注',
  `cat_status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1显示，0不显示',
  PRIMARY KEY (`cid`),
  KEY `cat_idname` (`cat_idname`)
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COMMENT='幻灯片分类表';

-- ----------------------------
-- Records of cmf_slide_cat
-- ----------------------------
INSERT INTO `cmf_slide_cat` VALUES ('1', '首页banner图集', 'portal_banner', '首页banner图集', '1');
INSERT INTO `cmf_slide_cat` VALUES ('2', '公司介绍', 'company_intro', '东阳正午阳光影视有限公司（简称“正午阳光影业”）成立于2011年，是一家具有专业水准的综合性影视机构，集影视策划制作发行，特效技术与后期制作、娱乐营销、演艺经纪为一体，拥有一支以孔笙、侯鸿亮、李雪、孙墨龙为创作主体的国内顶级制作团队。', '1');
INSERT INTO `cmf_slide_cat` VALUES ('3', '影视剧目', 'hot_series', '在首页展示的热播剧目', '1');
INSERT INTO `cmf_slide_cat` VALUES ('4', '明星档案', 'star_archives', '代表公司在首页轮播展示的明星们', '1');
INSERT INTO `cmf_slide_cat` VALUES ('5', '男明星', 'star_archives_male', '公司所有的男明星', '1');
INSERT INTO `cmf_slide_cat` VALUES ('6', '女明星', 'star_archives_female', '公司所有的女明星', '1');

-- ----------------------------
-- Table structure for cmf_terms
-- ----------------------------
DROP TABLE IF EXISTS `cmf_terms`;
CREATE TABLE `cmf_terms` (
  `term_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '分类id',
  `name` varchar(200) DEFAULT NULL COMMENT '分类名称',
  `slug` varchar(200) DEFAULT '',
  `taxonomy` varchar(32) DEFAULT NULL COMMENT '分类类型',
  `description` longtext COMMENT '分类描述',
  `parent` bigint(20) unsigned DEFAULT '0' COMMENT '分类父id',
  `count` bigint(20) DEFAULT '0' COMMENT '分类文章数',
  `path` varchar(500) DEFAULT NULL COMMENT '分类层级关系路径',
  `seo_title` varchar(500) DEFAULT NULL,
  `seo_keywords` varchar(500) DEFAULT NULL,
  `seo_description` varchar(500) DEFAULT NULL,
  `list_tpl` varchar(50) DEFAULT NULL COMMENT '分类列表模板',
  `one_tpl` varchar(50) DEFAULT NULL COMMENT '分类文章页模板',
  `listorder` int(5) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1发布，0不发布',
  PRIMARY KEY (`term_id`)
) ENGINE=MyISAM AUTO_INCREMENT=43 DEFAULT CHARSET=utf8 COMMENT='Portal 文章分类表';

-- ----------------------------
-- Records of cmf_terms
-- ----------------------------
INSERT INTO `cmf_terms` VALUES ('5', '明 星 档 案', '', 'article', '', '0', '0', '0-5', '', '', '', 'starslist', 'star', '20', '1');
INSERT INTO `cmf_terms` VALUES ('3', '公司资讯', '', 'article', '公司资讯公司资讯', '0', '0', '0-3', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('12', '相关影像', '', 'picture', '', '9', '0', '0-5-9-12', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('11', '明星档期', '', 'article', '', '9', '0', '0-5-9-11', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('10', '相关新闻', '', 'article', '', '9', '0', '0-5-9-10', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('9', '郭涛', '', 'article', '', '5', '0', '0-5-9', '', '', '', 'list', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('13', '刘奕君', '', 'article', '', '5', '0', '0-5-13', '', '', '', 'list', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('14', '相关新闻', '', 'article', '', '13', '0', '0-5-13-14', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('15', '明星档期', '', 'article', '', '13', '0', '0-5-13-15', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('16', '相关影像', '', 'picture', '', '13', '0', '0-5-13-16', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('17', '赵达', '', 'article', '赵达', '5', '0', '0-5-17', '', '', '', 'list', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('18', '岳阳', '', 'article', '岳阳', '5', '0', '0-5-18', '', '', '', 'starslist', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('19', '靳东', '', 'article', '靳东', '5', '0', '0-5-19', '', '', '', 'starslist', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('20', '郭晓然', '', 'article', '郭晓然', '5', '0', '0-5-20', '', '', '', 'starslist', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('21', '张陆', '', 'article', '张陆', '5', '0', '0-5-21', '', '', '', 'starslist', 'star', '0', '1');
INSERT INTO `cmf_terms` VALUES ('22', '相关新闻', '', 'article', '相关新闻', '17', '0', '0-5-17-22', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('23', '明星档期', '', 'article', '明星档期', '17', '0', '0-5-17-23', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('24', '相关影像', '', 'picture', '相关影像', '17', '0', '0-5-17-24', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('25', '相关影像', '', 'picture', '相关影像', '18', '0', '0-5-18-25', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('26', '相关影像', '', 'picture', '相关影像', '19', '0', '0-5-19-26', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('27', '相关影像', '', 'article', '相关影像', '20', '0', '0-5-20-27', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('28', '相关影像', '', 'article', '相关影像', '21', '0', '0-5-21-28', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('29', '相关新闻', '', 'article', '相关新闻', '18', '0', '0-5-18-29', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('30', '相关新闻', '', 'article', '相关新闻', '19', '0', '0-5-19-30', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('31', '相关新闻', '', 'article', '相关新闻', '20', '0', '0-5-20-31', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('32', '相关新闻', '', 'article', '相关新闻', '21', '0', '0-5-21-32', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('33', ' 明星档期', '', 'article', ' 明星档期', '21', '0', '0-5-21-33', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('34', ' 明星档期', '', 'article', ' 明星档期', '20', '0', '0-5-20-34', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('35', ' 明星档期', '', 'article', '', '18', '0', '0-5-18-35', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('36', ' 明星档期', '', 'article', '', '19', '0', '0-5-19-36', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('37', '影视剧集', '', 'article', '每个分类下面具有子分类', '0', '0', '0-37', '', '', '', 'list', 'article', '10', '1');
INSERT INTO `cmf_terms` VALUES ('39', '琅琊榜', '', 'article', '', '37', '0', '0-37-39', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('40', '剧情简介', '', 'article', '', '39', '0', '0-37-39-40', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('41', '拍摄花絮', '', 'article', '', '39', '0', '0-37-39-41', '', '', '', 'list', 'article', '0', '1');
INSERT INTO `cmf_terms` VALUES ('42', '相关新闻', '', 'article', '', '39', '0', '0-37-39-42', '', '', '', 'list', 'article', '0', '1');

-- ----------------------------
-- Table structure for cmf_term_relationships
-- ----------------------------
DROP TABLE IF EXISTS `cmf_term_relationships`;
CREATE TABLE `cmf_term_relationships` (
  `tid` bigint(20) NOT NULL AUTO_INCREMENT,
  `object_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT 'posts表里文章id',
  `term_id` bigint(20) unsigned NOT NULL DEFAULT '0' COMMENT '分类id',
  `listorder` int(10) NOT NULL DEFAULT '0' COMMENT '排序',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态，1发布，0不发布',
  PRIMARY KEY (`tid`),
  KEY `term_taxonomy_id` (`term_id`)
) ENGINE=MyISAM AUTO_INCREMENT=22 DEFAULT CHARSET=utf8 COMMENT='Portal 文章分类对应表';

-- ----------------------------
-- Records of cmf_term_relationships
-- ----------------------------
INSERT INTO `cmf_term_relationships` VALUES ('1', '1', '3', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('4', '5', '9', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('6', '6', '13', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('7', '7', '17', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('8', '8', '18', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('9', '9', '19', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('10', '10', '20', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('11', '11', '21', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('12', '12', '10', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('13', '13', '11', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('14', '14', '11', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('15', '15', '12', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('16', '17', '39', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('17', '18', '40', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('18', '19', '41', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('19', '20', '41', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('20', '21', '42', '0', '1');
INSERT INTO `cmf_term_relationships` VALUES ('21', '22', '12', '0', '1');

-- ----------------------------
-- Table structure for cmf_users
-- ----------------------------
DROP TABLE IF EXISTS `cmf_users`;
CREATE TABLE `cmf_users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) NOT NULL DEFAULT '' COMMENT '用户名',
  `user_pass` varchar(64) NOT NULL DEFAULT '' COMMENT '登录密码；sp_password加密',
  `user_nicename` varchar(50) NOT NULL DEFAULT '' COMMENT '用户美名',
  `user_email` varchar(100) NOT NULL DEFAULT '' COMMENT '登录邮箱',
  `user_url` varchar(100) NOT NULL DEFAULT '' COMMENT '用户个人网站',
  `avatar` varchar(255) DEFAULT NULL COMMENT '用户头像，相对于upload/avatar目录',
  `sex` smallint(1) DEFAULT '0' COMMENT '性别；0：保密，1：男；2：女',
  `birthday` date DEFAULT NULL COMMENT '生日',
  `signature` varchar(255) DEFAULT NULL COMMENT '个性签名',
  `last_login_ip` varchar(16) DEFAULT NULL COMMENT '最后登录ip',
  `last_login_time` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '最后登录时间',
  `create_time` datetime NOT NULL DEFAULT '2000-01-01 00:00:00' COMMENT '注册时间',
  `user_activation_key` varchar(60) NOT NULL DEFAULT '' COMMENT '激活码',
  `user_status` int(11) NOT NULL DEFAULT '1' COMMENT '用户状态 0：禁用； 1：正常 ；2：未验证',
  `score` int(11) NOT NULL DEFAULT '0' COMMENT '用户积分',
  `user_type` smallint(1) DEFAULT '1' COMMENT '用户类型，1:admin ;2:会员',
  `coin` int(11) NOT NULL DEFAULT '0' COMMENT '金币',
  `mobile` varchar(20) NOT NULL DEFAULT '' COMMENT '手机号',
  PRIMARY KEY (`id`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='用户表';

-- ----------------------------
-- Records of cmf_users
-- ----------------------------
INSERT INTO `cmf_users` VALUES ('1', 'admin', '###01dddc81d0df3e6eede7c19d6b1975a0', 'admin', 'admin@qq.com', '', null, '0', null, null, '127.0.0.1', '2016-05-15 20:37:26', '2016-04-02 10:35:34', '', '1', '0', '1', '0', '');

-- ----------------------------
-- Table structure for cmf_user_favorites
-- ----------------------------
DROP TABLE IF EXISTS `cmf_user_favorites`;
CREATE TABLE `cmf_user_favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `uid` bigint(20) DEFAULT NULL COMMENT '用户 id',
  `title` varchar(255) DEFAULT NULL COMMENT '收藏内容的标题',
  `url` varchar(255) DEFAULT NULL COMMENT '收藏内容的原文地址，不带域名',
  `description` varchar(500) DEFAULT NULL COMMENT '收藏内容的描述',
  `table` varchar(50) DEFAULT NULL COMMENT '收藏实体以前所在表，不带前缀',
  `object_id` int(11) DEFAULT NULL COMMENT '收藏内容原来的主键id',
  `createtime` int(11) DEFAULT NULL COMMENT '收藏时间',
  PRIMARY KEY (`id`),
  KEY `uid` (`uid`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='用户收藏表';

-- ----------------------------
-- Records of cmf_user_favorites
-- ----------------------------
