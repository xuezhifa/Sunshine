<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($site_name); ?>-<?php echo ($post_title); ?> </title>
	<meta name="keywords" content="<?php echo ($seo_keywords); ?>" />
	<meta name="description" content="<?php echo ($seo_description); ?>">
    
	<?php  function _sp_helloworld(){ echo "hello ThinkCMF!"; } function _sp_helloworld2(){ echo "hello ThinkCMF2!"; } function _sp_helloworld3(){ echo "hello ThinkCMF3!"; } ?>
	<?php $portal_index_lastnews="2"; $portal_hot_articles="1,2"; $portal_last_post="1,2"; $tmpl=sp_get_theme_path(); $default_home_slides=array( array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/1.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/2.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/3.jpg", "slide_url"=>"", ), ); ?>
	<meta name="author" content="zwsunshine">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge，chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

   	<!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

	<link rel="icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">

	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/style.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/animate.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrtk.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/homepage-z.css" rel="stylesheet" type="text/css" />
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/fix.css" rel="stylesheet" type="text/css">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/innerpageStyle.css" rel="stylesheet" type="text/css">
	<!-- jQuery start --> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/jquery.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/wow.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/lansige.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrscroll.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jquery.slides.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/Parallax/stellar.min.js" type="text/javascript" charset="utf-8"></script>
	<!--[if IE]>
	<style type="text/css">
	.english {
		font-family: arial !important;
	}
	</style>
	<![endif]-->
	
</head>
<body>
<?php echo hook('body_start');?>
 <div id="header" class="mini">
   <div class="content">
    <a href="/sunshine/" id="logo">
      <img src="/sunshine/themes/simplebootx/Public/statics/UIimages/logo.png"  height="40"/>
    </a>
	<?php
 $effected_id="nav"; $filetpl ="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $ul_class="subnav" ; $li_class="navitem" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="clear"></div>
    </div>
 </div>

<div id="newslist-topPic" data-stellar-ratio="0.3">
	<img width="100%" src="/sunshine/themes/simplebootx/Public/statics/style/images/newslist-topbg.jpg"/>
</div>

<!--news start by z-->
<section class="z-news">
	<div class="z-news-title wow animated fadeInUp">
		<div class="en-title english">NEWS</div>
		<div class="zh-title">公&nbsp;司&nbsp;资&nbsp;讯</div>
		<div class="bottline"></div>
	</div>
	<!--news list con by z-->
	<div class="z-news-list">
		<?php $lists = sp_sql_posts_paged("cid:$cat_id;order:post_date DESC;",10); ?>
		<?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $smeta=json_decode($vo['smeta'], true);$d = $vo['post_date']; ?>
		<!--one news cell by z-->
		<div data-href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>" class="z-news-cell cursor-pointer wow animated fadeInUp">
			<div class="newscontent  text-center">
				<div class="z-cell-date">
					<div class="zcell-d-m"><?php echo date('m',strtotime($d)); ?>/<?php echo date('d',strtotime($d)); ?></div>
					<div class="zcell-year"><?php echo date('Y',strtotime($d)); ?></div>
				</div>
				<div class="z-cell-content">
					<div class="zcell-title"><?php echo ($vo['post_title']); ?></div>
					<div class="zcell-brief"><?php echo ($vo['post_excerpt']); ?></div>
				</div>
				<i class="icon-angle-right fr"></i>
			</div>
		</div><?php endforeach; endif; else: echo "" ;endif; ?>
		
	</div>
	<div class="list-pagination">
		<ul class="display">
			<li><</li>
			<li>1</li>
			<li>2</li>
			<li>3</li>
			<li>4</li>
			<li>></li>
		</ul>
		<ul><?php echo ($lists['page']); ?></ul>
	</div>
</section>
<!--news end by z-->

<div class="container tc-main display">
	
    <div class="pg-opt pin">
        <div class="container">
            <h2><?php echo ($name); ?></h2>
        </div>
    </div>
    <div class="row">
		<div class="span9">
			<div class="">
				<?php $lists = sp_sql_posts_paged("cid:$cat_id;order:post_date DESC;",10); ?>
				<?php if(is_array($lists['posts'])): $i = 0; $__LIST__ = $lists['posts'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $smeta=json_decode($vo['smeta'], true); ?>
				
				<div class="list-boxes">
					<h2><a href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>"><?php echo ($vo["post_title"]); ?></a></h2>
					<p><?php echo (msubstr($vo["post_excerpt"],0,256)); ?></p>
					<div>
						<div class="pull-left">
							<div class="list-actions">
							<a href="javascript:;"><i class="fa fa-eye"></i><span><?php echo ($vo["post_hits"]); ?></span></a>
							<a href="<?php echo U('article/do_like',array('id'=>$vo['object_id']));?>" class="js-count-btn"><i class="fa fa-thumbs-up"></i><span class="count"><?php echo ($vo["post_like"]); ?></span></a>
							<a href="<?php echo U('user/favorite/do_favorite',array('id'=>$vo['object_id']));?>" class="js-favorite-btn" data-title="<?php echo ($vo["post_title"]); ?>" data-url="<?php echo U('portal/article/index',array('id'=>$vo['tid']));?>" data-key="<?php echo sp_get_favorite_key('posts',$vo['object_id']);?>">
								<i class="fa fa-star-o"></i>
							</a>
							</div>
						</div>
						<a class="btn btn-warning pull-right" href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>">查看更多</a>
					</div>
				</div><?php endforeach; endif; else: echo "" ;endif; ?>
				
			</div>
			<div class="pagination"><ul><?php echo ($lists['page']); ?></ul></div>
		</div>
		<div class="span3">
			<div class="tc-box first-box">
			<div class="headtitle">
          		<h2>分享</h2>
          	</div>
			<div class="bdsharebuttonbox"><a href="#" class="bds_more" data-cmd="more"></a><a href="#" class="bds_tsina" data-cmd="tsina" title="分享到新浪微博"></a><a href="#" class="bds_tqq" data-cmd="tqq" title="分享到腾讯微博"></a><a href="#" class="bds_qzone" data-cmd="qzone" title="分享到QQ空间"></a><a href="#" class="bds_tqf" data-cmd="tqf" title="分享到腾讯朋友"></a><a href="#" class="bds_renren" data-cmd="renren" title="分享到人人网"></a><a href="#" class="bds_youdao" data-cmd="youdao" title="分享到有道云笔记"></a></div>
			<script>window._bd_share_config={"common":{"bdSnsKey":{},"bdText":"","bdMini":"2","bdMiniList":false,"bdPic":"","bdStyle":"2","bdSize":"32"},"share":{}};with(document)0[(getElementsByTagName('head')[0]||body).appendChild(createElement('script')).src='http://bdimg.share.baidu.com/static/api/js/share.js?v=86835285.js?cdnversion='+~(-new Date()/36e5)];</script>          
        	</div>
        	
        	<div class="tc-box">
	        	<div class="headtitle">
	        		<h2>热门文章</h2>
	        	</div>
	        	<div class="ranking">
	        		<?php $hot_articles=sp_sql_posts("cid:$portal_hot_articles;field:post_title,post_excerpt,tid,term_id,smeta;order:post_hits desc;limit:5;"); ?>
		        	<ul class="unstyled">
		        		<?php if(is_array($hot_articles)): foreach($hot_articles as $key=>$vo): $top=$key<3?"top3":""; ?>
							<li class="<?php echo ($top); ?>"><i><?php echo ($key+1); ?></i><a title="<?php echo ($vo["post_title"]); ?>" href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>"><?php echo ($vo["post_title"]); ?></a></li><?php endforeach; endif; ?>
					</ul>
				</div>
			</div>
			<div class="tc-box">
	        	<div class="headtitle">
	        		<h2>最新评论</h2>
	        	</div>
	        	<div class="comment-ranking">
	        		<?php $last_comments=sp_get_comments("field:*;limit:0,5;order:createtime desc;"); ?>
	        		<?php if(is_array($last_comments)): foreach($last_comments as $key=>$vo): ?><div class="comment-ranking-inner">
	                        <i class="fa fa-comment"></i>
	                        <a href="<?php echo U('user/index/index',array('id'=>$vo['uid']));?>"><?php echo ($vo["full_name"]); ?>:</a>
	                        <span><?php echo ($vo["content"]); ?></span>
	                        <a href="/sunshine/<?php echo ($vo["url"]); ?>#comment<?php echo ($vo["id"]); ?>">查看原文</a>
	                        <span class="comment-time"><?php echo date('m月d日  H:i',strtotime($vo['createtime']));?></span>
	                    </div><?php endforeach; endif; ?>
                </div>
			</div>
			
			<div class="tc-box">
	        	<div class="headtitle">
	        		<h2>最新加入</h2>
	        	</div>
	        	<?php $last_users=sp_get_users("field:*;limit:0,8;order:create_time desc;"); ?>
	        	<ul class="list-unstyled tc-photos margin-bottom-30">
	        		<?php if(is_array($last_users)): foreach($last_users as $key=>$vo): ?><li>
	                    <a href="<?php echo U('user/index/index',array('id'=>$vo['id']));?>">
	                    <img alt="" src="<?php echo U('user/public/avatar',array('id'=>$vo['id']));?>">
	                    </a>
                    </li><?php endforeach; endif; ?>
                </ul>
			</div>
			
			<div class="tc-box">
	        	<div class="headtitle">
	        		<h2>最新发布</h2>
	        	</div>
	        	<?php $last_post=sp_sql_posts("cid:$portal_last_post;field:post_title,post_excerpt,tid,term_id,smeta;order:listorder asc;limit:4;"); ?>
	        	<div class="posts">
	        		<?php if(is_array($last_post)): foreach($last_post as $key=>$vo): $smeta=json_decode($vo['smeta'],true); ?>
			        	<dl class="dl-horizontal">
				            <dt>
					            <a class="img-wraper" href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>">
					            	<?php if(empty($smeta['thumb'])): ?><img src="/sunshine/themes/simplebootx/Public/images/default_tupian4.png" class="img-responsive" alt="<?php echo ($vo["post_title"]); ?>"/>
									<?php else: ?> 
										<img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" class="img-responsive img-thumbnail" alt="<?php echo ($vo["post_title"]); ?>" /><?php endif; ?>
					            </a>
				            </dt>
				            <dd><a href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>"><?php echo msubstr($vo['post_excerpt'],0,32);?></a></dd>
				        </dl><?php endforeach; endif; ?>
		        </div>
			</div>
			
			<?php $ad=sp_getad("portal_list_right_aside"); ?>
			<?php if(!empty($ad)): ?><div class="tc-box">
	        	<div class="headtitle">
	        		<h2>赞助商</h2>
	        	</div>
	        	<div>
		        	<?php echo ($ad); ?>
		        </div>
			</div><?php endif; ?>
		</div>
    </div>
    
    
    <!-- Footer ================================================== -->
<?php echo hook('footer');?>
<footer>
  <div class="z-foot">
  	<div class="z-foot-reposition ">
  		<ul class="z-foot-con">
		<li data-wow-delay=".0s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;"><img src="/sunshine/themes/simplebootx/Public/statics/images/3.png"></li>
		<li data-wow-delay=".1s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">电子邮件：MAIL@DAYLIGHT.COM</li>
		<li data-wow-delay=".2s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">联系电话：010-86665441</li>
		<li data-wow-delay=".3s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">公司地址：北京市朝阳区八里庄1号莱锦创意产业园CF29</li>
	</ul>
	<div class="zhixun">
		<a data-href="#" class="weixin" target="_blank">
			<span class="qrcode">
				<img src="/sunshine/themes/simplebootx/Public/statics/images/qrcode.png">
				<i id="triangle-down" class="sanjiao icon-caret-down"></i>
			</span>
		</a>
		<a data-href="#" class="sq cursor-pointer" target="_blank"></a> 
	</div>
  	</div>
  </div>
  <div class="z-footend" style="height: 60px;width: 100%;background: #FFF;">
  	<div class="z-footend-text">
  		Copyright&nbsp;(c)&nbsp;2016&nbsp;daylight.com&nbsp;&nbsp;&nbsp;&nbsp;京ICP备13015845号-1
  	</div>
  </div>
</footer>
<div id="back-top">
	<img src="/sunshine/themes/simplebootx/Public/statics/style/images/back-topImg.png"/>
</div>
<?php echo ($site_tongji); ?>

</div>

<!-- Footer ================================================== -->
<?php echo hook('footer');?>
<footer>
  <div class="z-foot">
  	<div class="z-foot-reposition ">
  		<ul class="z-foot-con">
		<li data-wow-delay=".0s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;"><img src="/sunshine/themes/simplebootx/Public/statics/images/3.png"></li>
		<li data-wow-delay=".1s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">电子邮件：MAIL@DAYLIGHT.COM</li>
		<li data-wow-delay=".2s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">联系电话：010-86665441</li>
		<li data-wow-delay=".3s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">公司地址：北京市朝阳区八里庄1号莱锦创意产业园CF29</li>
	</ul>
	<div class="zhixun">
		<a data-href="#" class="weixin" target="_blank">
			<span class="qrcode">
				<img src="/sunshine/themes/simplebootx/Public/statics/images/qrcode.png">
				<i id="triangle-down" class="sanjiao icon-caret-down"></i>
			</span>
		</a>
		<a data-href="#" class="sq cursor-pointer" target="_blank"></a> 
	</div>
  	</div>
  </div>
  <div class="z-footend" style="height: 60px;width: 100%;background: #FFF;">
  	<div class="z-footend-text">
  		Copyright&nbsp;(c)&nbsp;2016&nbsp;daylight.com&nbsp;&nbsp;&nbsp;&nbsp;京ICP备13015845号-1
  	</div>
  </div>
</footer>
<div id="back-top">
	<img src="/sunshine/themes/simplebootx/Public/statics/style/images/back-topImg.png"/>
</div>
<?php echo ($site_tongji); ?>

    
<!-- JavaScript -->
<script type="text/javascript" src="/sunshine/themes/simplebootx/Public/statics/style/js/jiaodiantu.js"></script>

<script>
$(function () {
	
	var t = false;
	setTimeout(function(){
		$('#skip-link').fadeOut(1000);
	},2000);

	/* 
	$('video')[0].addEventListener('canplaythrough',function(){
		if(!t){
			t = true;
			$('#skip-link').fadeOut(1000);
		}
	});*/
	
    $(".rslides").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
    });
	
	$(document).on('scroll',function(  ){
		if( $(document).scrollTop()!=0 ){
			$('#back-top').fadeIn();
		}else{
			$('#back-top').fadeOut();
		}
	}).trigger('scroll');
	
	$('#back-top').click(function(){
		$('html,body').animate({scrollTop: '0px'}, 500);
	});
	
	$(document).on('click','[data-href]',function(){
		var href = $(this).attr("data-href");
		href&&window.open(href,"_parent");
	});
});
</script>
</body>
</html>