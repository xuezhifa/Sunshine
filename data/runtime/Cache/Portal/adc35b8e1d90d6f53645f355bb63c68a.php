<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
	<html>
	<head>
		<title><?php echo ($site_name); ?>-<?php echo ($post_title); ?> </title>
		<meta name="keywords" content="<?php echo ($post_keywords); ?>" />
		<meta name="description" content="<?php echo ($post_excerpt); ?>">
		
	<?php  function _sp_helloworld(){ echo "hello ThinkCMF!"; } function _sp_helloworld2(){ echo "hello ThinkCMF2!"; } function _sp_helloworld3(){ echo "hello ThinkCMF3!"; } ?>
	<?php $portal_index_lastnews="2"; $portal_hot_articles="1,2"; $portal_last_post="1,2"; $tmpl=sp_get_theme_path(); $default_home_slides=array( array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/1.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/2.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/3.jpg", "slide_url"=>"", ), ); ?>
	<meta name="author" content="zwsunshine">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge，chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

   	<!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

	<link rel="icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">

	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/style.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/animate.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrtk.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/homepage-z.css" rel="stylesheet" type="text/css" />
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/fix.css" rel="stylesheet" type="text/css">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/innerpageStyle.css" rel="stylesheet" type="text/css">
	<!-- jQuery start --> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/jquery.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/wow.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/lansige.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrscroll.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jquery.slides.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/Parallax/stellar.min.js" type="text/javascript" charset="utf-8"></script>
	<!--[if IE]>
	<style type="text/css">
	.english {
		font-family: arial !important;
	}
	</style>
	<![endif]-->
	
		<link rel="stylesheet" type="text/css" href="/sunshine/themes/simplebootx/Public/statics/style/css/video-style.css"/>
	</head>
<body class="">
<?php echo hook('body_start');?>
 <div id="header" class="mini">
   <div class="content">
    <a href="/sunshine/" id="logo">
      <img src="/sunshine/themes/simplebootx/Public/statics/UIimages/logo.png"  height="40"/>
    </a>
	<?php
 $effected_id="nav"; $filetpl ="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $ul_class="subnav" ; $li_class="navitem" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="clear"></div>
    </div>
 </div>
	<?php $index_cid=37; $records = sp_get_child_terms($index_cid); ?>
	
	<section class="video-sets">
	
	<?php if(is_array($records)): $i = 0; $__LIST__ = $records;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $cid=$vo["term_id"]; $video = sp_sql_posts("cid:$cid;field:post_date,post_title,post_content,post_excerpt,tid,smeta;order:listorder asc;limit:1;")[0]; $smeta = json_decode($video["smeta"],true); $records_childs = sp_get_child_terms($cid); $baseid = -1; $huaxuid= -1; $newsid = -1; foreach($records_childs as $key=>$voo ) { if("剧情简介" ==$voo["name"]){ $baseid = $voo["term_id"]; }else if("拍摄花絮" ==$voo["name"]){ $huaxuid = $voo["term_id"]; }else if("相关新闻" ==$voo["name"]){ $newsid = $voo["term_id"]; } } ?>

		<!-- 相关新闻 -->
		<?php  ?>
		<div class="container video-item">
			<div class="cont-desc">
				<div class="product-details">				
					<div class="grid images_3_of_2">
						<img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt="<?php echo ($video["post_title"]); ?>" />
					</div>
					<?php echo ($video["post_content"]); ?>
					<div class="clear"></div>
				</div>
				
				<p  class="ff-flag down open"><!--open or close -->
					<span class="f1"><span style="color:#333">展开</span><br><i class="icon-angle-down"></i></span>
				</p>
				<section class="z-news product_desc">
					<div class="star-tabs">
						<ul class="tab-control">
							<li id="tab-control1" class="tab-btn tab-btn-active">剧情简介</li>
							<li id="tab-control2" class="tab-btn">拍摄花絮</li>
							<li id="tab-control3" class="tab-btn">相关新闻</li>
						</ul>
						<ul class="tab-contents">
							<!-- 剧情简介 -->
							<?php $base = sp_sql_posts("cid:$baseid;field:post_date,post_title,post_content,post_excerpt,tid,smeta;order:listorder asc;limit:1;")[0]; $basesmeta = json_decode($base["smeta"],true); $basephotos = $basesmeta["photo"]; ?>
							<li id="tab-content1" class="tab-content">
								<div class="row introduce-juqing" style="visibility: visible; animation-name: fadeInUp;">
									<div class="news-detail-content">
										<!--这里是富文本正文-->
										<?php echo ($base["post_content"]); ?>
										<?php if(is_array($basephotos)): $i = 0; $__LIST__ = $basephotos;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$p): $mod = ($i % 2 );++$i;?><img src="<?php echo sp_get_asset_upload_path($p['url']);?>" alt="<?php echo ($p["alt"]); ?>"><?php endforeach; endif; else: echo "" ;endif; ?>
									</div>
								</div>
							</li>
							<!-- 拍摄花絮 -->
							<?php $huaxu=sp_sql_posts("cid:$huaxuid;field:post_date,post_title,post_excerpt,tid,smeta;order:listorder asc;limit:4;"); ?>
							<li id="tab-content2" class="tab-content">
								<?php if(is_array($huaxu)): foreach($huaxu as $key=>$vooo): ?><!--one news cell by z-->
								<div class="z-news-cell cursor-pointer">
									<div class="newscontent  text-center"  data-href="<?php echo leuu('article/index',array('id'=>$vooo['tid'],'cid'=>$vooo['term_id']));?>">
										<div class="z-cell-date">
											<div class="zcell-d-m"><?php echo date('m',strtotime($d)); ?>/<?php echo date('d',strtotime($d)); ?></div>
											<div class="zcell-year"><?php echo date('Y',strtotime($d)); ?></div>
										</div>
										<div class="z-cell-content">
											<div class="zcell-title"><?php echo ($vooo['post_title']); ?></div>
											<div class="zcell-brief"><?php echo ($vooo['post_excerpt']); ?></div>
										</div>
										<i class="icon-angle-right fr"></i>
									</div>
								</div><?php endforeach; endif; ?>
							</li>
							<!-- 拍摄花絮 -->
							<?php $news=sp_sql_posts("cid:$newsid;field:post_date,post_title,post_excerpt,tid,smeta;order:listorder asc;limit:4;"); ?>
							<li id="tab-content3" class="tab-content">
								<?php if(is_array($news)): foreach($news as $key=>$voooo): ?><!--one news cell by z-->
								<div class="z-news-cell  cursor-pointer">
									<div class="newscontent  text-center"  data-href="<?php echo leuu('article/index',array('id'=>$voooo['tid'],'cid'=>$voooo['term_id']));?>">
										<div class="z-cell-date">
											<div class="zcell-d-m"><?php echo date('m',strtotime($d)); ?>/<?php echo date('d',strtotime($d)); ?></div>
											<div class="zcell-year"><?php echo date('Y',strtotime($d)); ?></div>
										</div>
										<div class="z-cell-content">
											<div class="zcell-title"><?php echo ($voooo['post_title']); ?></div>
											<div class="zcell-brief"><?php echo ($voooo['post_excerpt']); ?></div>
										</div>
										<i class="icon-angle-right fr"></i>
									</div>
								</div><?php endforeach; endif; ?>
							</li>
						</ul>
					</div>
				</section>
				
				<p  class="ff-flag up"><!--open or close -->
					<span class="f2"><span style="color:#333">收起</span><br><i class="icon-angle-up"></i></span>
				</p>
		   </div>
		</div><?php endforeach; endif; else: echo "" ;endif; ?>
	</section>

<!-- Footer ================================================== -->
<?php echo hook('footer');?>
<footer>
  <div class="z-foot">
  	<div class="z-foot-reposition ">
  		<ul class="z-foot-con">
		<li data-wow-delay=".0s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;"><img src="/sunshine/themes/simplebootx/Public/statics/images/3.png"></li>
		<li data-wow-delay=".1s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">电子邮件：MAIL@DAYLIGHT.COM</li>
		<li data-wow-delay=".2s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">联系电话：010-86665441</li>
		<li data-wow-delay=".3s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">公司地址：北京市朝阳区八里庄1号莱锦创意产业园CF29</li>
	</ul>
	<div class="zhixun">
		<a data-href="#" class="weixin" target="_blank">
			<span class="qrcode">
				<img src="/sunshine/themes/simplebootx/Public/statics/images/qrcode.png">
				<i id="triangle-down" class="sanjiao icon-caret-down"></i>
			</span>
		</a>
		<a data-href="#" class="sq cursor-pointer" target="_blank"></a> 
	</div>
  	</div>
  </div>
  <div class="z-footend" style="height: 60px;width: 100%;background: #FFF;">
  	<div class="z-footend-text">
  		Copyright&nbsp;(c)&nbsp;2016&nbsp;daylight.com&nbsp;&nbsp;&nbsp;&nbsp;京ICP备13015845号-1
  	</div>
  </div>
</footer>
<div id="back-top">
	<img src="/sunshine/themes/simplebootx/Public/statics/style/images/back-topImg.png"/>
</div>
<?php echo ($site_tongji); ?>

<script type="text/javascript" src="/sunshine/themes/simplebootx/Public/statics/style/js/jiaodiantu.js"></script>

<script>
$(function () {
	
	var t = false;
	setTimeout(function(){
		$('#skip-link').fadeOut(1000);
	},2000);

	/* 
	$('video')[0].addEventListener('canplaythrough',function(){
		if(!t){
			t = true;
			$('#skip-link').fadeOut(1000);
		}
	});*/
	
    $(".rslides").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
    });
	
	$(document).on('scroll',function(  ){
		if( $(document).scrollTop()!=0 ){
			$('#back-top').fadeIn();
		}else{
			$('#back-top').fadeOut();
		}
	}).trigger('scroll');
	
	$('#back-top').click(function(){
		$('html,body').animate({scrollTop: '0px'}, 500);
	});
	
	$(document).on('click','[data-href]',function(){
		var href = $(this).attr("data-href");
		href&&window.open(href,"_parent");
	});
});
</script>
<script>

$(function() {
	$(".z-news").each(function(){
		var scope = $(this);
		scope.find("#tab-control1").click(function(){
			if(scope.find("#tab-content1").is(":hidden")){
				scope.find(".tab-btn").removeClass("tab-btn-active");
				$(this).addClass("tab-btn-active");
				scope.find(".tab-content").fadeOut(300);
				scope.find("#tab-content1").delay(200).fadeIn(300);
			}
		});
		scope.find("#tab-control2").click(function(){
			if(scope.find("#tab-content2").is(":hidden")){
				scope.find(".tab-btn").removeClass("tab-btn-active");
				$(this).addClass("tab-btn-active");
				scope.find(".tab-content").fadeOut(300);
				scope.find("#tab-content2").delay(200).fadeIn(300);
			}
		});
		scope.find("#tab-control3").click(function(){
			if(scope.find("#tab-content3").is(":hidden")){
				scope.find(".tab-btn").removeClass("tab-btn-active");
				$(this).addClass("tab-btn-active");
				scope.find(".tab-content").fadeOut(300);
				scope.find("#tab-content3").delay(200).fadeIn(300);
			}
		});
		
		
		//lightbox
		scope.find("#tab-content3 img").click(function(){
			scope.find(".popup-lightbox").fadeIn(400);
		});
		scope.find(".light-box-close").click(function(){
			scope.find(".popup-lightbox").fadeOut(400);
		});
		
	});
	
	$(document).on("click",".ff-flag",function(){
		var  el 	= $(this);
		var  box   	= el.closest(".video-item");
		var  desc	= box.find(".product_desc");
		var  up 	= box.find(".ff-flag.up");
		var  down 	= box.find(".ff-flag.down");
		el.removeClass("open");
		if(el.hasClass("up")){
			down.addClass("open");
			desc.fadeOut();
		}else{
			up.addClass("open");
			desc.fadeIn();
		}
	});
	
});

</script>
</body>
</html>