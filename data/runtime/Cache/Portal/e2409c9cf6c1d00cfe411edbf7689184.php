<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
	<html>
	<head>
		<title><?php echo ($site_name); ?>-<?php echo ($post_title); ?> </title>
		<meta name="keywords" content="<?php echo ($post_keywords); ?>" />
		<meta name="description" content="<?php echo ($post_excerpt); ?>">
		
	<?php  function _sp_helloworld(){ echo "hello ThinkCMF!"; } function _sp_helloworld2(){ echo "hello ThinkCMF2!"; } function _sp_helloworld3(){ echo "hello ThinkCMF3!"; } ?>
	<?php $portal_index_lastnews="2"; $portal_hot_articles="1,2"; $portal_last_post="1,2"; $tmpl=sp_get_theme_path(); $default_home_slides=array( array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/1.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/2.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/3.jpg", "slide_url"=>"", ), ); ?>
	<meta name="author" content="zwsunshine">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge，chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

   	<!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

	<link rel="icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">

	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/style.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/animate.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrtk.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/homepage-z.css" rel="stylesheet" type="text/css" />
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/fix.css" rel="stylesheet" type="text/css">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/innerpageStyle.css" rel="stylesheet" type="text/css">
	<!-- jQuery start --> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/jquery.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/wow.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/lansige.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrscroll.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jquery.slides.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/Parallax/stellar.min.js" type="text/javascript" charset="utf-8"></script>
	<!--[if IE]>
	<style type="text/css">
	.english {
		font-family: arial !important;
	}
	</style>
	<![endif]-->
	
		<style>
			#article_content img{height:auto !important}
		</style>
	</head>
<body class="">
<?php echo hook('body_start');?>
 <div id="header" class="mini">
   <div class="content">
    <a href="/sunshine/" id="logo">
      <img src="/sunshine/themes/simplebootx/Public/statics/UIimages/logo.png"  height="40"/>
    </a>
	<?php
 $effected_id="nav"; $filetpl ="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $ul_class="subnav" ; $li_class="navitem" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="clear"></div>
    </div>
 </div>

<!-- 公司介绍 start -->
<section class="introduce" style="background-color: #f6f6f6;margin-top: 0px  !important;">
	<div class="container text-center">
		<div class="l-introduce-title wow animated fadeInUp">
			<div class="en-title english">ABOUT&nbsp;US</div>
			<div class="zh-title">公&nbsp;司&nbsp;介&nbsp;绍</div>
			<div class="bottline"></div>
		</div>
		<?php $smeta=json_decode($smeta,true); ?>
		<div id="aboutus-content" class="row introduce-content wow animated fadeInUp">
			<img width="551px" src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>"/>
			<p><?php echo ($post_content); ?></p>
		</div>
	</div>
</section><!-- 公司介绍 end -->

<!-- Footer ================================================== -->
<?php echo hook('footer');?>
<footer>
  <div class="z-foot">
  	<div class="z-foot-reposition ">
  		<ul class="z-foot-con">
		<li data-wow-delay=".0s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;"><img src="/sunshine/themes/simplebootx/Public/statics/images/3.png"></li>
		<li data-wow-delay=".1s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">电子邮件：MAIL@DAYLIGHT.COM</li>
		<li data-wow-delay=".2s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">联系电话：010-86665441</li>
		<li data-wow-delay=".3s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">公司地址：北京市朝阳区八里庄1号莱锦创意产业园CF29</li>
	</ul>
	<div class="zhixun">
		<a data-href="#" class="weixin" target="_blank">
			<span class="qrcode">
				<img src="/sunshine/themes/simplebootx/Public/statics/images/qrcode.png">
				<i id="triangle-down" class="sanjiao icon-caret-down"></i>
			</span>
		</a>
		<a data-href="#" class="sq cursor-pointer" target="_blank"></a> 
	</div>
  	</div>
  </div>
  <div class="z-footend" style="height: 60px;width: 100%;background: #FFF;">
  	<div class="z-footend-text">
  		Copyright&nbsp;(c)&nbsp;2016&nbsp;daylight.com&nbsp;&nbsp;&nbsp;&nbsp;京ICP备13015845号-1
  	</div>
  </div>
</footer>
<div id="back-top">
	<img src="/sunshine/themes/simplebootx/Public/statics/style/images/back-topImg.png"/>
</div>
<?php echo ($site_tongji); ?>

<script type="text/javascript" src="/sunshine/themes/simplebootx/Public/statics/style/js/jiaodiantu.js"></script>

<script>
$(function () {
	
	var t = false;
	setTimeout(function(){
		$('#skip-link').fadeOut(1000);
	},2000);

	/* 
	$('video')[0].addEventListener('canplaythrough',function(){
		if(!t){
			t = true;
			$('#skip-link').fadeOut(1000);
		}
	});*/
	
    $(".rslides").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
    });
	
	$(document).on('scroll',function(  ){
		if( $(document).scrollTop()!=0 ){
			$('#back-top').fadeIn();
		}else{
			$('#back-top').fadeOut();
		}
	}).trigger('scroll');
	
	$('#back-top').click(function(){
		$('html,body').animate({scrollTop: '0px'}, 500);
	});
	
	$(document).on('click','[data-href]',function(){
		var href = $(this).attr("data-href");
		href&&window.open(href,"_parent");
	});
});
</script>
</body>
</html>