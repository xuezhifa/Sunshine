<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($site_name); ?>-<?php echo ($post_title); ?> </title>
	<meta name="keywords" content="<?php echo ($post_keywords); ?>" />
	<meta name="description" content="<?php echo ($post_excerpt); ?>">
	
	<?php  function _sp_helloworld(){ echo "hello ThinkCMF!"; } function _sp_helloworld2(){ echo "hello ThinkCMF2!"; } function _sp_helloworld3(){ echo "hello ThinkCMF3!"; } ?>
	<?php $portal_index_lastnews="2"; $portal_hot_articles="1,2"; $portal_last_post="1,2"; $tmpl=sp_get_theme_path(); $default_home_slides=array( array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/1.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/2.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/3.jpg", "slide_url"=>"", ), ); ?>
	<meta name="author" content="zwsunshine">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge，chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

   	<!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

	<link rel="icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">

	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/style.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/animate.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrtk.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/homepage-z.css" rel="stylesheet" type="text/css" />
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/fix.css" rel="stylesheet" type="text/css">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/innerpageStyle.css" rel="stylesheet" type="text/css">
	<!-- jQuery start --> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/jquery.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/wow.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/lansige.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrscroll.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jquery.slides.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/Parallax/stellar.min.js" type="text/javascript" charset="utf-8"></script>
	<!--[if IE]>
	<style type="text/css">
	.english {
		font-family: arial !important;
	}
	</style>
	<![endif]-->
	
	<style>
		#article_content img{height:auto !important}
	</style>
	
	<script src="/sunshine/themes/simplebootx/Public/statics/lightbox/layer/layer.min.js"></script>
	<link  href="/sunshine/themes/simplebootx/Public/statics/videojs/reveal.css" rel="stylesheet" />
</head>
<body class="">
<?php echo hook('body_start');?>
 <div id="header" class="mini">
   <div class="content">
    <a href="/sunshine/" id="logo">
      <img src="/sunshine/themes/simplebootx/Public/statics/UIimages/logo.png"  height="40"/>
    </a>
	<?php
 $effected_id="nav"; $filetpl ="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $ul_class="subnav" ; $li_class="navitem" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="clear"></div>
    </div>
 </div>

<?php $index_cid=I("cid"); $records = sp_get_child_terms($index_cid); $newid = -1; $scheduleid = -1; $imageid = -1; foreach($records as $key=>$vo ) { if("相关新闻" ==$vo["name"]){ $newid = $vo["term_id"]; }else if("明星档期" ==$vo["name"]){ $scheduleid = $vo["term_id"]; }else if("相关影像" ==$vo["name"]){ $imageid = $vo["term_id"]; } } ?>
	<!-- ljj0179@qingdaomedia.com -->
<!--news start by z-->
<section class="z-news">
	<div class="star-profile">
		<?php echo ($post_content); ?>
	</div>
	<div class="star-profile display">
		<div class="star-photo-con">
			<img src="http://1805595e.all123.net/sunshine/themes/simplebootx/Public/statics/style/images/star-photo.png" alt="" />
		</div>
		<div class="star-info-con">
			<div class="star-info-CNname"><span>王凯</span></div>
			<div class="star-info-Enname"><span>Wang Kai</span></div>
			<div class="star-Constellation"><span class="starinfo-left">星座</span><span class="starinfo-right">狮子座</span></div>
			<div class="star-blood"><span class="starinfo-left">血型</span><span class="starinfo-right">A </span></div>
			<div class="star-height"><span class="starinfo-left">身高</span><span class="starinfo-right">182cm </span></div>
			<div class="star-weight"><span class="starinfo-left">体重</span><span class="starinfo-right">70kg</span></div>
			<div class="star-addr"><span class="starinfo-left">出生地</span><span class="starinfo-right">湖北武汉 </span></div>
			<div class="birth"><span class="starinfo-left">生日</span><span class="starinfo-right">1982年8月18日（农历六月二十九）</span></div>
			<div class="star-colleage"><span class="starinfo-left">毕业院校</span><span class="starinfo-right">中央戏剧学院03级表演本科</span></div>
			<div class="star-works"><span class="starinfo-left">代表作品</span><span class="starinfo-right">新神探联盟、北平无战事、伪装者、琅琊榜 </span></div>
			<div class="star-awards"><div class="starinfo-left">主要成就</div><div class="starinfo-right">“风从东方来”娱乐影响力盛典“年度最具人气电视剧演员”奖<br>
				 2015国剧盛典内地最具人气演员<br>
				 2015国剧盛典年度演技飞跃演员<br></div>
			</div>
			<div class="star-shop-con">
				<img class="cursor-pointer" src="http://1805595e.all123.net/sunshine/themes/simplebootx/Public/statics/style/images/star-shop.png"/>
			</div>
		</div>
	</div>
	<div class="star-tabs">
		<ul class="tab-control">
			<li id="tab-control1" class="tab-btn tab-btn-active">相关新闻</li>
			<li id="tab-control2" class="tab-btn">明星档期</li>
			<li id="tab-control3" class="tab-btn">相关影像</li>
		</ul>
		<ul class="tab-contents">
			<li id="tab-content1" class="tab-content">
				<?php $index_cid=$newid; $lastnews=sp_sql_posts("cid:$index_cid;field:post_date,post_title,post_excerpt,tid,smeta;order:listorder asc;limit:4;"); ?>
				<?php if(is_array($lastnews)): foreach($lastnews as $key=>$vo): $smeta=json_decode($vo['smeta'],true);$d = $vo['post_date']; ?>
				<!--one news cell by z-->
				<div class="z-news-cell">
					<div class="newscontent  text-center"  data-href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>">
						<div class="z-cell-date">
							<div class="zcell-d-m"><?php echo date('m',strtotime($d)); ?>/<?php echo date('d',strtotime($d)); ?></div>
							<div class="zcell-year"><?php echo date('Y',strtotime($d)); ?></div>
						</div>
						<div class="z-cell-content">
							<div class="zcell-title"><?php echo ($vo['post_title']); ?></div>
							<div class="zcell-brief"><?php echo ($vo['post_excerpt']); ?></div>
						</div>
						<i class="icon-angle-right fr"></i>
					</div>
				</div><?php endforeach; endif; ?>
			</li>
			<li id="tab-content2" class="tab-content">
				<?php $index_cid=$scheduleid; $lastnews=sp_sql_posts("cid:$index_cid;field:post_date,post_title,post_excerpt,tid,smeta;order:listorder asc;limit:4;"); ?>
				<?php if(is_array($lastnews)): $i = 0; $__LIST__ = $lastnews;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $smeta=json_decode($vo['smeta'],true);$d = $vo['post_date']; ?>
				<?php if($key==0){ $class=" fisrt-star-date "; }else if($key==(count($lastnews)-1)){ $class=" last-star-date "; } else{ $class="  "; } ?>
				<!--中间档期-->
				<div class="z-star-date <?php echo ($class); ?>">
					<div class="newscontent  text-center">
						<div class="z-cell-date">
							<div class="zcell-d-m"><?php echo date('m',strtotime($d)); ?>/<?php echo date('d',strtotime($d)); ?></div>
							<div class="zcell-year"><?php echo date('Y',strtotime($d)); ?></div>
						</div>
						<div class="starline-line"></div>
						<div class="starline-reddot"></div>
						
						<div class="z-star-date-right">
							<div class="zcell-title"><?php echo ($vo['post_title']); ?></div>
						</div>
					</div>
				</div><?php endforeach; endif; else: echo "" ;endif; ?>
			</li>
			<li id="tab-content3" class="tab-content yingxiang">
				<?php $index_cid=$imageid; $lastnews=sp_sql_posts("cid:$index_cid;field:post_date,post_title,post_content,post_excerpt,tid,smeta;order:listorder asc;limit:4;"); ?>
				<?php if(is_array($lastnews)): $i = 0; $__LIST__ = $lastnews;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i; $smeta=json_decode($vo['smeta'],true); ?>
				<?php $photos = $smeta["photo"]; ?>
				<!--one news cell by z-->
				<div  class="male-star-item">
					<img src="<?php echo sp_get_asset_upload_path($smeta['thumb']);?>" alt="<?php echo ($vo["post_title"]); ?>" />
					<div class="star-name"><?php echo ($vo["post_title"]); ?></div>
					<?php if(!empty($photos)): ?><div id="imgs" class="display image-container">
						<?php if(is_array($photos)): foreach($photos as $key=>$vo): ?><img src="<?php echo sp_get_asset_upload_path($vo['url']);?>"/><?php endforeach; endif; ?>
						</div><?php endif; ?>
					<?php if(strpos($vo["post_content"],"video-js")&&strpos($vo["post_content"],"edui-upload-video") ) { echo $vo["post_content"]; } ?>
				</div><?php endforeach; endif; else: echo "" ;endif; ?>
			</li>
		</ul>
	</div>
</section>
<!--news end by z-->
<div id="myModal" class="reveal-modal">
	<div id="video-view" style=" text-align: center;"></div>
	<div class="xubox_imgbar" style="text-align: center;">
	<span class="xubox_imgtit"><a class="xubox_imgtit-title"></a></span>
	</div>
	<span class="xubox_setwin"><a class="close-reveal-modal xubox_close xulayer_png32 xubox_close0 xubox_close1" href="javascript:;" style=""></a></span>
</div>

<!-- Footer ================================================== -->
<?php echo hook('footer');?>
<footer>
  <div class="z-foot">
  	<div class="z-foot-reposition ">
  		<ul class="z-foot-con">
		<li data-wow-delay=".0s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;"><img src="/sunshine/themes/simplebootx/Public/statics/images/3.png"></li>
		<li data-wow-delay=".1s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">电子邮件：MAIL@DAYLIGHT.COM</li>
		<li data-wow-delay=".2s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">联系电话：010-86665441</li>
		<li data-wow-delay=".3s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">公司地址：北京市朝阳区八里庄1号莱锦创意产业园CF29</li>
	</ul>
	<div class="zhixun">
		<a data-href="#" class="weixin" target="_blank">
			<span class="qrcode">
				<img src="/sunshine/themes/simplebootx/Public/statics/images/qrcode.png">
				<i id="triangle-down" class="sanjiao icon-caret-down"></i>
			</span>
		</a>
		<a data-href="#" class="sq cursor-pointer" target="_blank"></a> 
	</div>
  	</div>
  </div>
  <div class="z-footend" style="height: 60px;width: 100%;background: #FFF;">
  	<div class="z-footend-text">
  		Copyright&nbsp;(c)&nbsp;2016&nbsp;daylight.com&nbsp;&nbsp;&nbsp;&nbsp;京ICP备13015845号-1
  	</div>
  </div>
</footer>
<div id="back-top">
	<img src="/sunshine/themes/simplebootx/Public/statics/style/images/back-topImg.png"/>
</div>
<?php echo ($site_tongji); ?>

<script type="text/javascript" src="/sunshine/themes/simplebootx/Public/statics/style/js/jiaodiantu.js"></script>

<script>
$(function () {
	
	var t = false;
	setTimeout(function(){
		$('#skip-link').fadeOut(1000);
	},2000);

	/* 
	$('video')[0].addEventListener('canplaythrough',function(){
		if(!t){
			t = true;
			$('#skip-link').fadeOut(1000);
		}
	});*/
	
    $(".rslides").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
    });
	
	$(document).on('scroll',function(  ){
		if( $(document).scrollTop()!=0 ){
			$('#back-top').fadeIn();
		}else{
			$('#back-top').fadeOut();
		}
	}).trigger('scroll');
	
	$('#back-top').click(function(){
		$('html,body').animate({scrollTop: '0px'}, 500);
	});
	
	$(document).on('click','[data-href]',function(){
		var href = $(this).attr("data-href");
		href&&window.open(href,"_parent");
	});
});
</script>
<script src="/sunshine/themes/simplebootx/Public/statics/videojs/jquery.reveal.js"></script>
<script src="/sunshine/themes/simplebootx/Public/statics/videojs/jquery.metadata.js"></script>
<script src="/sunshine/themes/simplebootx/Public/statics/videojs/jquery.media.js"></script> 
<script>
;!function(){
	layer.use('extend/layer.ext.js', function(){
		//初始加载即调用，所以需放在ext回调里
		
		$('.male-star-item').click(function(){
			var box = $(this);
			var imgsBox = $(this).find('.image-container');
			var title =  $(this).find('.star-name').html();
			if(imgsBox.length){
				layer.photos({
					page: { //直接获取页面指定区域的图片，他与上述异步不可共存，只能择用其一。
						parent:imgsBox,  //图片的父元素选择器，如'#imsbox',
						start: 0, //初始显示的图片序号，默认0
						title:title //相册标题
					}
				});
			}else{
				var video = box.find(".video-js");
				
				$("#myModal").find(".xubox_imgtit-title").html(title);
				$("#video-view").html(video.parent("p").html());
				$('#myModal').reveal();
				var h = $('#myModal').height()-$("#video-view").height();
				$("#video-view").css("margin-top",h/2);
				$("#video-view").find("video")[0].play();
			}
			
		});

	});
}();
</script>

<script>

$(function() {

	$("#tab-control1").click(function(){
		if($("#tab-content1").is(":hidden")){
			$(".tab-btn").removeClass("tab-btn-active");
			$(this).addClass("tab-btn-active");
			$(".tab-content").fadeOut(300);
			$("#tab-content1").delay(200).fadeIn(300);
		}
	});
	$("#tab-control2").click(function(){
		if($("#tab-content2").is(":hidden")){
			$(".tab-btn").removeClass("tab-btn-active");
			$(this).addClass("tab-btn-active");
			$(".tab-content").fadeOut(300);
			$("#tab-content2").delay(200).fadeIn(300);
		}
	});
	$("#tab-control3").click(function(){
		if($("#tab-content3").is(":hidden")){
			$(".tab-btn").removeClass("tab-btn-active");
			$(this).addClass("tab-btn-active");
			$(".tab-content").fadeOut(300);
			$("#tab-content3").delay(200).fadeIn(300);
		}
	});
	
	
	//lightbox
	$("#tab-content3 img").click(function(){
		$(".popup-lightbox").fadeIn(400);
	});
	$(".light-box-close").click(function(){
		$(".popup-lightbox").fadeOut(400);
	});
});

</script>


</body>
</html>