<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
	<html>
	<head>
		<title><?php echo ($site_seo_title); ?></title>
		<meta name="keywords" content="<?php echo ($site_seo_keywords); ?>" />
		<meta name="description" content="<?php echo ($site_seo_description); ?>">
		
	<?php  function _sp_helloworld(){ echo "hello ThinkCMF!"; } function _sp_helloworld2(){ echo "hello ThinkCMF2!"; } function _sp_helloworld3(){ echo "hello ThinkCMF3!"; } ?>
	<?php $portal_index_lastnews="2"; $portal_hot_articles="1,2"; $portal_last_post="1,2"; $tmpl=sp_get_theme_path(); $default_home_slides=array( array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/1.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/2.jpg", "slide_url"=>"", ), array( "slide_name"=>"ThinkCMFX2.1.0发布啦！", "slide_pic"=>$tmpl."Public/images/demo/3.jpg", "slide_url"=>"", ), ); ?>
	<meta name="author" content="zwsunshine">
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge，chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    
    <!-- Set render engine for 360 browser -->
    <meta name="renderer" content="webkit">

   	<!-- No Baidu Siteapp-->
    <meta http-equiv="Cache-Control" content="no-siteapp"/>

	<link rel="icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">
	<link rel="shortcut icon" href="/sunshine/themes/simplebootx/Public/statics/images/favicon.ico" type="image/x-icon">

	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/style.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/animate.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrtk.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/fonts/css/font-awesome.min.css" rel="stylesheet">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/homepage-z.css" rel="stylesheet" type="text/css" />
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/fix.css" rel="stylesheet" type="text/css">
	<link href="/sunshine/themes/simplebootx/Public/statics/style/css/innerpageStyle.css" rel="stylesheet" type="text/css">
	<!-- jQuery start --> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/jquery.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/wow.min.js"></script> 
	<script src="/sunshine/themes/simplebootx/Public/statics/style/js/lansige.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jCarouselLite/lrscroll.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/jquery.slides.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="/sunshine/themes/simplebootx/Public/statics/libs/Parallax/stellar.min.js" type="text/javascript" charset="utf-8"></script>
	<!--[if IE]>
	<style type="text/css">
	.english {
		font-family: arial !important;
	}
	</style>
	<![endif]-->
	
		<link href="/sunshine/themes/simplebootx/Public/css/slippry/slippry.css" rel="stylesheet">
		<style>
			.caption-wraper{position: absolute;left:50%;bottom:2em;}
			.caption-wraper .caption{
			position: relative;left:-50%;
			background-color: rgba(0, 0, 0, 0.54);
			padding: 0.4em 1em;
			color:#fff;
			-webkit-border-radius: 1.2em;
			-moz-border-radius: 1.2em;
			-ms-border-radius: 1.2em;
			-o-border-radius: 1.2em;
			border-radius: 1.2em;
			}
			@media (max-width: 767px){
				.sy-box{margin: 12px -20px 0 -20px;}
				.caption-wraper{left:0;bottom: 0.4em;}
				.caption-wraper .caption{
				left: 0;
				padding: 0.2em 0.4em;
				font-size: 0.92em;
				-webkit-border-radius: 0;
				-moz-border-radius: 0;
				-ms-border-radius: 0;
				-o-border-radius: 0;
				border-radius: 0;}
			}
		</style>
	</head>
<body >
<?php echo hook('body_start');?>
 <div id="header" class="">
   <div class="content">
    <a href="/sunshine/" id="logo">
      <img src="/sunshine/themes/simplebootx/Public/statics/UIimages/logo.png"  height="40"/>
    </a>
	<?php
 $effected_id="nav"; $filetpl ="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $foldertpl="<a href='\$href' target='\$target' class='cursor-pointer'>\$label</a>"; $ul_class="subnav" ; $li_class="navitem" ; $style="nav"; $showlevel=6; $dropdown='dropdown'; echo sp_get_menu("main",$effected_id,$filetpl,$foldertpl,$ul_class,$li_class,$style,$showlevel,$dropdown); ?>
    <div class="clear"></div>
    </div>
 </div>

<!-- banner start -->
<div id="swiper-top1" data-stellar-ratio="0.3" class="text-center" style="position: fixed;width:100%;top: 0;left: 0;z-index: -200;">
	<?php $home_slides=sp_getslide("portal_banner"); ?>
	<ul id="rslides" class="rslides list-unstyled">
		<?php if(is_array($home_slides)): foreach($home_slides as $key=>$vo): ?><li>
			<div class="banner" data-href="<?php echo ($vo["slide_url"]); ?>">
				<img class="banner-image" src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>"  alt="<?php echo ($vo["slide_name"]); ?>"/>
			</div>
		</li><?php endforeach; endif; ?>
	</ul>
	<div class="sliderArrow icon-angle-down" style="transform: translateY(0px); opacity: 1;"></div>
</div>
<!-- banner End -->

<!-- 公司介绍 start -->
<section class="introduce" style="background-color: #f6f6f6;">
	<div class="container text-center">
		<div class="l-introduce-title wow animated fadeInUp">
			<div class="en-title english">ABOUT&nbsp;US</div>
			<div class="zh-title">公&nbsp;司&nbsp;介&nbsp;绍</div>
			<div class="bottline"></div>
		</div>
		<?php $company_intros=sp_getslide("company_intro"); ?>
		<div class="row">
			<?php if(is_array($company_intros)): $i = 0; $__LIST__ = $company_intros;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><figure data-wow-delay="0.<?php echo ($key); ?>s" data-href="<?php echo ($vo["slide_url"]); ?>" class="wow animated fadeInUp">
				<img class="hot-video img-hover-scale" src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>"   alt="<?php echo ($vo["slide_name"]); ?>" />
			</figure><?php endforeach; endif; else: echo "" ;endif; ?>
		</div>
		<div class="row introduce-content wow animated fadeInUp">
			<?php echo $company_intros[0]["cat_remark"]; ?>
		</div>
		<div class="intro z-news-more wow animated fadeInUp">
			<div class="english more-z-news-btn cursor-pointer" data-href='<?php echo leuu("portal/page/index",array("id"=>2));?>'>
				MORE
			<i class="icon-angle-right fr"></i>
			</div>
		</div>
	</div>
</section><!-- 公司介绍 end -->

<!-- fuwu start -->
<section class="hot-series">
	<div class="container text-center">
		<div class="l-introduce-title wow animated fadeInUp">
			<div class="en-title english">HOT&nbsp;SERIES</div>
			<div class="zh-title">影&nbsp;视&nbsp;剧&nbsp;目</div>
			<div class="bottline"></div>
		</div>		
	</div>
	<?php $hot_series=sp_getslide("hot_series"); ?>
    <div class="row">
        <div class="series-content">
			<?php if(is_array($hot_series)): $i = 0; $__LIST__ = $hot_series;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><figure data-wow-delay="0.<?php echo ($key); ?>s" data-href="<?php echo ($vo["slide_url"]); ?>"  class="wow animated fadeInUp">
					<img class="hot-video img-hover-scale" src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>" alt="<?php echo ($vo["slide_name"]); ?>" />
					<div class="hot-video-desc cursor-pointer">
						<span class="video-desc">VIEW DETAILS</span>
					</div>
				</figure><?php endforeach; endif; else: echo "" ;endif; ?>
        </div>
	</div>
	<div class="z-news-more wow animated fadeInUp">
		<div class="english more-z-news-btn cursor-pointer">
			MORE
		<i class="icon-angle-right fr"></i>
		</div>
	</div>
</section><!-- fuwu end -->
<section class="z-stars text-center" style="width: 100%;position: relative;left: 0;top: 0;">
	<!--top title-->
	<div class="z-stars-title wow animated fadeInUp">
		<div class="en-title english">STARS</div>
		<div class="zh-title">明&nbsp;星&nbsp;档&nbsp;案</div>
		<div class="bottline"></div>
	</div>
	
	<?php $star_archives=sp_getslide("star_archives"); ?>
	<!--sliders-->
	<ul id="rslides-stars" class="rslides list-unstyled wow animated fadeInUp">
	<?php if(is_array($star_archives)): $i = 0; $__LIST__ = $star_archives;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$vo): $mod = ($i % 2 );++$i;?><li class="hover-parent">
			<div class="star-con">
				<div class="star-frame">
					<div class="star-photo"><img src="/sunshine/themes/simplebootx/Public/statics/UIimages/idol-2.jpg"/></div>
					<div class="star-zhName"><?php echo ($vo['slide_name']); ?></div>
					<div class="star-enName"><?php echo ($vo['slide_des']); ?></div>
					<div class="star-desc"><?php echo ($vo['slide_content']); ?></div>
				</div>
			</div>
			<div class="star-banner">
				<img class="banner-image" src="<?php echo sp_get_asset_upload_path($vo['slide_pic']);?>" alt="<?php echo ($vo["slide_name"]); ?>"/>
			</div>
		</li><?php endforeach; endif; else: echo "" ;endif; ?>
	</ul>
</section>
<!--news start by z-->

<section class="z-news">
	<div class="z-news-title wow animated fadeInUp">
		<div class="en-title english">NEWS</div>
		<div class="zh-title">公&nbsp;司&nbsp;资&nbsp;讯</div>
		<div class="bottline"></div>
	</div>
	<?php $index_cid=3; $lastnews=sp_sql_posts("cid:$index_cid;field:post_date,post_title,post_excerpt,tid,smeta;order:listorder asc;limit:4;"); ?>
	<!--news list con by z-->
	<div class="z-news-list">
		<?php if(is_array($lastnews)): foreach($lastnews as $key=>$vo): $smeta=json_decode($vo['smeta'],true);$d = $vo['post_date']; ?>
		<!--one news cell by z-->
		<div class="z-news-cell wow animated fadeInUp">
			<div class="newscontent  text-center"  data-href="<?php echo leuu('article/index',array('id'=>$vo['tid'],'cid'=>$vo['term_id']));?>">
				<div class="z-cell-date">
					<div class="zcell-d-m"><?php echo date('m',strtotime($d)); ?>/<?php echo date('d',strtotime($d)); ?></div>
					<div class="zcell-year"><?php echo date('Y',strtotime($d)); ?></div>
				</div>
				<div class="z-cell-content">
					<div class="zcell-title"><?php echo ($vo['post_title']); ?></div>
					<div class="zcell-brief"><?php echo ($vo['post_excerpt']); ?></div>
				</div>
				<i class="icon-angle-right fr"></i>
			</div>
		</div><?php endforeach; endif; ?>
	</div>
	<div class="z-news-more cursor-pointer wow animated fadeInUp">
		<div class="english more-z-news-btn cursor-pointer" data-href="<?php echo leuu('list/index',array('id'=>$index_cid));?>">
				MORE
			<i class="icon-angle-right fr"></i>
		</div>
	</div>
</section>
<!--news end by z-->

<!-- Footer ================================================== -->
<?php echo hook('footer');?>
<footer>
  <div class="z-foot">
  	<div class="z-foot-reposition ">
  		<ul class="z-foot-con">
		<li data-wow-delay=".0s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0s; animation-name: fadeInUp;"><img src="/sunshine/themes/simplebootx/Public/statics/images/3.png"></li>
		<li data-wow-delay=".1s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.1s; animation-name: fadeInUp;">电子邮件：MAIL@DAYLIGHT.COM</li>
		<li data-wow-delay=".2s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.2s; animation-name: fadeInUp;">联系电话：010-86665441</li>
		<li data-wow-delay=".3s" class="wow animated fadeInUp animated" style="visibility: visible; animation-delay: 0.3s; animation-name: fadeInUp;">公司地址：北京市朝阳区八里庄1号莱锦创意产业园CF29</li>
	</ul>
	<div class="zhixun">
		<a data-href="#" class="weixin" target="_blank">
			<span class="qrcode">
				<img src="/sunshine/themes/simplebootx/Public/statics/images/qrcode.png">
				<i id="triangle-down" class="sanjiao icon-caret-down"></i>
			</span>
		</a>
		<a data-href="#" class="sq cursor-pointer" target="_blank"></a> 
	</div>
  	</div>
  </div>
  <div class="z-footend" style="height: 60px;width: 100%;background: #FFF;">
  	<div class="z-footend-text">
  		Copyright&nbsp;(c)&nbsp;2016&nbsp;daylight.com&nbsp;&nbsp;&nbsp;&nbsp;京ICP备13015845号-1
  	</div>
  </div>
</footer>
<div id="back-top">
	<img src="/sunshine/themes/simplebootx/Public/statics/style/images/back-topImg.png"/>
</div>
<?php echo ($site_tongji); ?>


<script type="text/javascript" src="/sunshine/themes/simplebootx/Public/statics/style/js/jiaodiantu.js"></script>

<script>
$(function () {
	
	var t = false;
	setTimeout(function(){
		$('#skip-link').fadeOut(1000);
	},2000);

	/* 
	$('video')[0].addEventListener('canplaythrough',function(){
		if(!t){
			t = true;
			$('#skip-link').fadeOut(1000);
		}
	});*/
	
    $(".rslides").responsiveSlides({
        auto: true,
        pager: false,
        nav: true,
        speed: 500,
    });
	
	$(document).on('scroll',function(  ){
		if( $(document).scrollTop()!=0 ){
			$('#back-top').fadeIn();
		}else{
			$('#back-top').fadeOut();
		}
	}).trigger('scroll');
	
	$('#back-top').click(function(){
		$('html,body').animate({scrollTop: '0px'}, 500);
	});
	
	$(document).on('click','[data-href]',function(){
		var href = $(this).attr("data-href");
		href&&window.open(href,"_parent");
	});
});
</script>
<script>
$(function(){

	try{
		wow = new WOW({animateClass: 'animated',offset:100});
		wow.init();
	}catch(e){
		//console.log.log(e);
	}
	
	$(document).on('scroll',function(  ){
		if( $(document).scrollTop()!=0 ){
			$("#header").addClass('mini');
		}else{
			$("#header").removeClass('mini');
		}
	}).trigger('scroll');
	
	var $w = $(window);
	function resize_line_height()
	{
		var h = $('.hot-series figure').height();
		$('.hot-series .video-desc,.hot-series .hot-video-desc').css("line-height",h+"px");
	}
	$w.on("resize",function(){
		var h = $w.height();
		$("#rslides,.banner").height(h);
		//$("#rslides").width(screen.width);
		$(".introduce").css("margin-top",h+"px");
		resize_line_height();
	});
	$w.trigger("resize");
	
	//视差滚动
	//$.stellar();
	var top = 0;
	var position = 0;
	var m = 2;
	$(window).scroll(function(){
		var st = $(window).scrollTop();
		if( st==0){
			position = top = 0;
			m = 2;
		}
		////console.log.log(st,111);
		var res = st-position;
		if( res >0 ){
			top +=(st-position)/++m;
		}else{
			top = st;
		}		
		position = st;
		$('#rslides').css({transform:"translateY(-"+top+"px)"});
		//transform: translateY(-150px);
		//$("#swiper-top1").stop().css("top",-(top-top*0.6) + "px");
		resize_line_height();
	});
})
</script>
<?php echo hook('footer_end');?>
</body>
</html>